package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TTagForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;
	
	public String[] getIds() {
		return this.ids;
	}
	
	public void setIds(String[] ids) {
		this.ids = ids;
	}

		
			
				
			private java.lang.String id;	
					
				
			private java.lang.Integer no;	
					
				
			private java.lang.String tagTxt;	
					
				
			private java.lang.String org;	
					
				
			private java.lang.String fatherId;	
				
						public java.lang.String getId() {
				return this.id;
			}
			
			public void setId(java.lang.String id) {
				this.id = id;
			}	
								public java.lang.Integer getNo() {
				return this.no;
			}
			
			public void setNo(java.lang.Integer no) {
				this.no = no;
			}	
								public java.lang.String getTagTxt() {
				return this.tagTxt;
			}
			
			public void setTagTxt(java.lang.String tagTxt) {
				this.tagTxt = tagTxt;
			}	
								public java.lang.String getOrg() {
				return this.org;
			}
			
			public void setOrg(java.lang.String org) {
				this.org = org;
			}	
								public java.lang.String getFatherId() {
				return this.fatherId;
			}
			
			public void setFatherId(java.lang.String fatherId) {
				this.fatherId = fatherId;
			}	
				
}

