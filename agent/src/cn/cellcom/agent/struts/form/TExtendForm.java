package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TExtendForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String id;

	private java.lang.Long createTime;

	private java.lang.String name;

	private java.lang.String pid;

	private java.lang.String type;

	private java.lang.String org;

	private java.lang.String url;

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(java.lang.Long createTime) {
		this.createTime = createTime;
	}

	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getPid() {
		return this.pid;
	}

	public void setPid(java.lang.String pid) {
		this.pid = pid;
	}

	public java.lang.String getType() {
		return this.type;
	}

	public void setType(java.lang.String type) {
		this.type = type;
	}

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

	public java.lang.String getUrl() {
		return this.url;
	}

	public void setUrl(java.lang.String url) {
		this.url = url;
	}

}
