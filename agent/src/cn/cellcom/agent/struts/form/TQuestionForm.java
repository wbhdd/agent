package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.common.AgentConstant;

public class TQuestionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String id;

	private java.lang.String insertTime;

	private java.lang.String no;

	private java.lang.String answer;

	private java.lang.String question;

	private java.lang.String org;

	private java.lang.String type;

	private int times;

	private String subject = AgentConstant.DEFAULT_ORG;

	private int level = 1;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public java.lang.String getType() {
		return type;
	}

	public void setType(java.lang.String type) {
		this.type = type;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(java.lang.String insertTime) {
		this.insertTime = insertTime;
	}

	public java.lang.String getNo() {
		return this.no;
	}

	public void setNo(java.lang.String no) {
		this.no = no;
	}

	public java.lang.String getAnswer() {
		return this.answer;
	}

	public void setAnswer(java.lang.String answer) {
		this.answer = answer;
	}

	public java.lang.String getQuestion() {
		return this.question;
	}

	public void setQuestion(java.lang.String question) {
		this.question = question;
	}

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

}
