package cn.cellcom.agent.struts.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.biz.TGroupBiz;
import cn.cellcom.agent.biz.TUserBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.hql.TGroupHql;
import cn.cellcom.agent.pojo.TGroup;
import cn.cellcom.agent.pojo.TGroupUser;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TGroupForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.util.ABean;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TGroupDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private TGroupBiz cbiz;
	
	private TUserBiz ubiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		List list = null;
		try {
			log.info("查询的语句：TGroupHql()");
			list = pbiz.pagination(mapping, form, req, new TGroupHql()).getDataResult();
			
			if("select2".equals(req.getParameter("from"))) {
				List<Map<String, String>> mlist = new ArrayList<Map<String, String>>();
				for (int i = 0; i < list.size(); i++) {
					TGroup group = (TGroup) list.get(i);
					Map<String, String> e = new HashMap<String, String>();
					e.put("id", group.getId());
					e.put("text", group.getName() + "[组号:" + CU.valueOf(group.getNo()) + "]");
					mlist.add(e);
				}
				Response.succ(rep, mlist);
				return null;
			} else {
				req.setAttribute(Env.DATA_NAME, list);
			}
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		// 存在读取数据库的配置

		return mapping.findForward("list");
	}

	/**
	 * 准备记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		return mapping.findForward("add");
	}

	/**
	 * 执行记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doAdd(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TGroupForm fm = (TGroupForm) form;
		Object obj = null;
		try {
			obj = cbiz.add(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "新增数据出现异常", e);
		}
		log.info("新增记录结果：" + obj);
		String message;
		if (obj == null || !(obj instanceof TGroup)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		} else {
			message = "记录添加成功";
		}

		add(mapping, fm, req, rep);
		// req.setAttribute("success", fm);
		req.setAttribute(Env.DATA_NAME, message);
		return mapping.findForward("add");
	}

	/**
	 * 准备修改记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward modify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TGroupForm fm = (TGroupForm) form;
		try {
			java.lang.String key = fm.getId();
			TGroup rf = (TGroup) cbiz.getDao().myGet(TGroup.class, key);
			if (rf == null) {
				req.setAttribute(Env.DATA_NAME, "读取数据错误");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, rf);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取数据错误", e, mapping, req);
		}
		return mapping.findForward("modify");
	}

	/**
	 * 执行记录修改
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doModify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TGroupForm fm = (TGroupForm) form;
		Object obj = null;
		try {
			obj = cbiz.modify(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "修改数据出现异常", e);
		}
		log.info("修改记录结果：" + obj);
		if (obj == null || !(obj instanceof TGroup)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		}
		req.setAttribute(Env.DATA_NAME, "记录修改成功");
		return mapping.findForward("success");
	}
	

	/**
	 * 查询已配置的坐席
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward agents(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		try {
			LogonSession ls = new LogonSession(req);
			TUser user = (TUser) ls.getLogonObject();
			String group = req.getParameter("id");
			//已经被该技能组配置的坐席
			List<TGroupUser> selectedAgentList = cbiz.getAgents(user.getPid(), group);
			//所有的坐席
			List<TUser> allAgentList = ubiz.getAgents(user.getPid(), null, AgentConstant.USER_STATUS_YES);
			//把坐席标记为已经选择
			for (int i = 0; selectedAgentList != null && i < selectedAgentList.size(); i++) {
				TGroupUser one = selectedAgentList.get(i);
				for (int j = 0; j < allAgentList.size(); j++) {
					TUser two = allAgentList.get(j);
					if(one.getUser().equals(two.getUsername())) {
						two.setTmp("xx");
					}
				}
			}
			
			List<ABean> beanList = BeanUtil.toABeanList(allAgentList, "username", "username,name,phone", "tmp");
			req.setAttribute("agentlist", beanList);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		
		return mapping.findForward("agents");
	}
	
	/**
	 * 绑定坐席到技能组
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doBindAgent(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TGroupForm fm = (TGroupForm) form;
		Object obj = null;
		try {
			LogonSession ls = new LogonSession(req);
			TUser user = (TUser) ls.getLogonObject();
			
			String[] agents = req.getParameterValues("agents");
			obj = cbiz.bindAgents(user.getOrg(), user.getPid(), fm.getId(), agents);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "修改数据出现异常", e);
		}
		log.info("修改记录结果：" + obj);
		if (obj == null || !obj.equals(Env.SUCCESS)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		}
		req.setAttribute(Env.DATA_NAME, "记录修改成功");
		return mapping.findForward("success");
	}

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = (TGroupBiz) cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}

	public void setUbiz(TUserBiz ubiz) {
		this.ubiz = ubiz;
	}
}
