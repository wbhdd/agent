package cn.cellcom.agent.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.hql.TLwHql;
import cn.cellcom.agent.pojo.TLw;
import cn.cellcom.agent.struts.form.TLwForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.file.IFileProcessor;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.port.ExportTemplate;
import cn.cellcom.jar.port.IDataPort;
import cn.cellcom.jar.port.JxlPort;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TLwDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private AbstractBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		List list = null;
		try {
			log.info("查询的语句：TLwHql()");
			list = pbiz.pagination(mapping, form, req, new TLwHql()).getDataResult();

			req.setAttribute(Env.DATA_NAME, list);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		req.setAttribute("statuslist", BeanUtil.startWith("TLw-status"));
		return mapping.findForward("list");
	}

	/**
	 * 准备修改记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward modify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TLwForm fm = (TLwForm) form;
		try {
			java.lang.String key = fm.getId();
			TLw rf = (TLw) cbiz.getDao().myGet(TLw.class, key);
			if (rf == null) {
				req.setAttribute(Env.DATA_NAME, "读取数据错误");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, rf);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取数据错误", e, mapping, req);
		}
		req.setAttribute("statuslist", BeanUtil.startWith("TLw-status"));
		return mapping.findForward("modify");
	}

	/**
	 * 执行记录修改
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doModify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TLwForm fm = (TLwForm) form;
		Object obj = null;
		try {
			obj = cbiz.modify(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "修改数据出现异常", e);
		}
		log.info("修改记录结果：" + obj);
		if (obj == null || !(obj instanceof TLw)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		}
		req.setAttribute(Env.DATA_NAME, "记录修改成功");
		return mapping.findForward("success");
	}

	/**
	 * 导出数据
	 * 
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doExport(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		try {
			TLwHql hql0 = new TLwHql();
			List list = this.cbiz.getDao().myList(hql0.getHql(req, form), hql0.getParaValue(req, form));
			if (list == null || list.size() < 1) {
				req.setAttribute(Env.DATA_NAME, "本次查询没有数据，所以不做导出");
				return mapping.findForward("success");
			}

			ExportTemplate template = new ExportTemplate() {
				private static final long serialVersionUID = 1L;

				@Override
				public void setRow(DataRow row, Object obj) {
					TLw pojo = (TLw) obj;
					row.add(CU.valueOf(pojo.getId()));
					row.add(CU.valueOf(pojo.getName()));
					row.add(CU.valueOf(pojo.getPhone()));
					row.add(CU.valueOf(pojo.getEmail()));
					row.add(CU.valueOf(pojo.getStatus()));
					row.add(CU.valueOf(pojo.getQq()));
					row.add(CU.valueOf(pojo.getMemo()));
					row.add(CU.valueOf(pojo.getCrm()));
					row.add(CU.valueOf(DT.getYMDHMS(pojo.getCreateTime())));

				}
			};
			String root3path3file = AgentEnv.EXPORT_PATH + CU.getSerial(".xls");
			template.setRoot3path3file(root3path3file);
			template.setContentTitle(new String[] { " id", " 姓名", " 联系电话", " Email", " 状态", " QQ", " 留言", " crmId", " 留言时间" });

			IDataPort port = new JxlPort();
			template.setDataList(list);
			port.export(template);

			IFileLoad fl = new FileLoad();
			fl.download(req, rep, this.servlet.getServletConfig(), root3path3file);
			IFileProcessor fp = new FileProcessor();
			fp.delete(root3path3file);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}

		LogUtil.e(this.clazz, "数据已经成功导出", null, mapping, req);
		return null;
	}

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}
}
