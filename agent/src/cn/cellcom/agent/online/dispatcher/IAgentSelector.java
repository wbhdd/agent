package cn.cellcom.agent.online.dispatcher;

import java.util.Comparator;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.wrapper.SessionWrapper;

/**
 * 选择坐席的接口
 * 
 * @author zhengyj
 * 
 */
public interface IAgentSelector {

	/**
	 * 判断坐席是否有效:
	 * 
	 * @param agent
	 * @param session
	 * @return
	 */
	boolean validateAgent(AgentClient agent, SessionWrapper session);

	/**
	 * 从可能的坐席列表中找到最合适的
	 * 
	 * @param possibleAgents
	 * @param session
	 * @return
	 */
	AgentClient bestAgentFrom(SessionWrapper session);

}
