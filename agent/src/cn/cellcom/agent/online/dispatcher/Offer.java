package cn.cellcom.agent.online.dispatcher;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.wrapper.SessionWrapper;

public class Offer {
	/**
	 * User associated with this Offer
	 */
	private SessionWrapper session;
	/**
	 * The (server) time the Offer was made to the agent, initialized in
	 * constructor.
	 */
	private Date offerTime;

	/**
	 * Flag indicating the offer has been cancelled or not.
	 */
	private boolean cancelled;

	/**
	 * Create an offer based on the request.
	 * 
	 * @param session
	 *            The request this offer is for
	 * @param queue
	 *            The request queue that is sending this offer to the agents.
	 * @param rejectionTimeout
	 *            the number of milliseconds to wait until expiring an agent
	 *            rejection.
	 */
	public Offer(SessionWrapper session) {
		this.session = session;
		offerTime = new Date();
		cancelled = false;
	}

	public SessionWrapper getSession() {
		return session;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public Date getOfferTime() {
		return offerTime;
	}

	public void invite(AgentClient agent) {
	}

	public void cancel() {
		cancelled = true;
	}

	public void addPendingSession(AgentClient AgentClient) {
	}
}
