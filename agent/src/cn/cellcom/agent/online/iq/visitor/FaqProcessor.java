package cn.cellcom.agent.online.iq.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.j2ee.AgentListener;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TQuestion;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class FaqProcessor extends AbstractChainedProcessor {

	private static final String NAME = "faq";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public FaqProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		List list = new ArrayList();
		String hql = "from " + TQuestion.class.getCanonicalName() + " where (pid = ?) and (type = ?) order by times desc";
		Object[] values = new Object[] { cf.getPid(), AgentConstant.QUESTION_TYPE.F.name() };
		try {
			list = AgentListener.getMgPageDao().myList(hql, values, 1, 10);
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
