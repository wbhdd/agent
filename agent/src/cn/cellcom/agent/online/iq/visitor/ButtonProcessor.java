package cn.cellcom.agent.online.iq.visitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TButtonBiz;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TButton;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class ButtonProcessor extends AbstractChainedProcessor {

	public static final String NAME = "button";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public ButtonProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		List<TButton> list = new ArrayList<TButton>();
		try {
			TButtonBiz bbiz = (TButtonBiz) bizs.get("bbiz");
			list = bbiz.getVisitorButtonList(cf.getPid(), cf.getChannel());
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
