package cn.cellcom.agent.online.iq;

import java.util.Map;

import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;

/**
 * 消息处理器
 * 
 * @author zhengyj
 * 
 */
public interface Processor {
	public ProcessorResult process(Map<String, AbstractBiz> bizs, Client client, VisitorForm message);
}
