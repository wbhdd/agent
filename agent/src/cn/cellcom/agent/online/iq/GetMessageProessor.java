package cn.cellcom.agent.online.iq;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.message.BizMessage;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.AU;

public class GetMessageProessor extends AbstractChainedProcessor {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public GetMessageProessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm message) {
		client.setLastActiveGet();

		List<BizMessage> mlist = client.getXMessage(50);
		if (log.isDebugEnabled()) {
			if (AU.isNotBlank(mlist)) {
				for (int i = 0; i < mlist.size(); i++) {
					log.debug("[{}] <RECEIVE_MESSAGE> [{}]", client.getId(), mlist.get(i));
				}
			}
		}
		return new ProcessorResult(true, this, mlist);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return StringUtils.isBlank(message.getEvent());
	}

}
