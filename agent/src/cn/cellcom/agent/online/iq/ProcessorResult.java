package cn.cellcom.agent.online.iq;

/**
 * 消息处理器
 * 
 * @author zhengyj
 * 
 */
public class ProcessorResult {
	private boolean success;
	private Processor processor;
	private Object data;

	public ProcessorResult(boolean result, Processor processor, Object data) {
		this.success = result;
		this.processor = processor;
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setResult(boolean result) {
		this.success = result;
	}

	public Processor getProcessor() {
		return processor;
	}

	public void setProcessor(Processor processor) {
		this.processor = processor;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
