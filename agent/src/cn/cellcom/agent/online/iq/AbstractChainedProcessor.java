package cn.cellcom.agent.online.iq;

import java.util.Map;

import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;

public abstract class AbstractChainedProcessor implements Processor {

	private Processor next;

	public AbstractChainedProcessor(Processor next) {
		super();
		this.next = next;
	}

	public ProcessorResult process(Map<String, AbstractBiz> bizs, Client client, VisitorForm message) {
		ProcessorResult result = null;
		if (isAcceptable(message)) {
			result = doProcess(bizs, client, message);
		} else {
			if (next != null) {
				result = next.process(bizs, client, message);
			}
		}
		return result;
	}

	public abstract ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm message);

	public abstract boolean isAcceptable(VisitorForm message);

}
