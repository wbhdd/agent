package cn.cellcom.agent.online.iq.agent;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TCrmBiz;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.jar.util.MyException;

public class ModifyCrmProcessor extends AbstractChainedProcessor {

	public static final String NAME = "modifyCrm";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public ModifyCrmProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		TCrmBiz cbiz = (TCrmBiz) bizs.get("cbiz");
		TCrm dest = null;
		try {
			dest = (TCrm) cbiz.getDao().myGet(TCrm.class, cf.getId());
			if (dest == null) {
				return new ProcessorResult(false, this, "");
			} else {
				log.info("被修改的pojo：" + ToString.toString(TCrm.class, dest, false));
				TCrm newOne = (TCrm) cf.getObject();
				CopyUtil.copy(newOne, dest, null, false);
				cbiz.getDao().myUpdate(dest);
			}
		} catch (MyException e) {
			log.error("[{}] get crmifo[{}]", client.getId(), cf.getCid());
			return new ProcessorResult(false, this, "");
		}
		return new ProcessorResult(true, this, dest);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
