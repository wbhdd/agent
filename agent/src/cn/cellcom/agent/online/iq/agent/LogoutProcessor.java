package cn.cellcom.agent.online.iq.agent;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;

public class LogoutProcessor extends AbstractChainedProcessor {

	private static final String NAME = MessageConstant.MESSAGE_EVENT.AG_LOGOUT.name();

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public LogoutProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		AgentClient ac = (AgentClient) client;
		log.info("[{}] logout by processor.", client.getId());
		ac.logout();
		return new ProcessorResult(true, this, "");
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
