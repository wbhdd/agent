package cn.cellcom.agent.online.iq.agent;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.j2ee.AgentListener;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TTrace;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.MyException;

public class CrmTraceProcessor extends AbstractChainedProcessor {

	public static final String NAME = "crmTrace";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public CrmTraceProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		List list = null;
		try {
			AgentClient ac = (AgentClient) client;
			String hql = "from " + TTrace.class.getCanonicalName() + " where (pid = ?) and (crm = ?) order by createTime desc";
			Object[] values = new Object[] { ac.getPid(), cf.getCid() };
			list = AgentListener.getMgDao().myList(hql, values);
		} catch (MyException e) {
			log.error("[{}] get crm trace[{}] fail.", client.getId(), cf.getCid());
			log.error("", e.getException());
			return new ProcessorResult(false, this, "");
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
