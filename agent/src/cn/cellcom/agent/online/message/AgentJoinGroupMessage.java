package cn.cellcom.agent.online.message;

import java.util.List;

import cn.cellcom.agent.pojo.TUser;

public class AgentJoinGroupMessage extends BizMessage {

	public AgentJoinGroupMessage(TUser sender, List<String> gs) {
		super.setObject(gs);
		super.setEvent(MessageConstant.MESSAGE_EVENT.NOTIFY);
		super.setNotify(MessageConstant.MESSAGE_NOTIFY.JOIN_GROUP);
	}

}
