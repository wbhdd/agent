package cn.cellcom.agent.online.message;


public class ErrorMessage extends BizMessage {

	public ErrorMessage(String receiver) {
		super.setReceiver(receiver);
		super.setEvent(MessageConstant.MESSAGE_EVENT.NOTIFY);
		super.setNotify(MessageConstant.MESSAGE_NOTIFY.ERROR);
	}
}
