package cn.cellcom.agent.online.message;

import java.util.List;

import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.pojo.TGroup;

/**
 * @author zhengyj
 * 
 */
public class GroupListMessage extends BizMessage {

	public GroupListMessage(SessionWrapper session, List<TGroup> list) {
		super.setPid(session.getSession().getPid());
		super.setSid(session.getId());
		super.setReceiver(session.getSession().getCrm());
		super.setEvent(MessageConstant.MESSAGE_EVENT.GROUP_LIST);
		super.setObject(list);
		super.setCid(session.getSession().getCrm());
	}
}
