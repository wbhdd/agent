package cn.cellcom.agent.online.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueueManager {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private static QueueManager manager = null;
	private Map<String, Queue> queues = new ConcurrentHashMap<String, Queue>();

	private QueueManager() {

	}

	public static QueueManager getInstance() {
		if (manager == null) {
			manager = new QueueManager();
		}
		return manager;
	}

	public Queue getQueue(String pid) {
		Queue q = queues.get(pid);
		if (q == null) {
			synchronized (("queue_" + pid).intern()) {
				if (q == null) {
					q = new Queue(pid);
					queues.put(pid, q);
					log.info("create session queue for pid " + pid);
				}
			}
		}
		return q;
	}
}
