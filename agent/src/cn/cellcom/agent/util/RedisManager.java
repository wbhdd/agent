package cn.cellcom.agent.util;

import org.apache.commons.lang.StringUtils;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.token.TokenEntity;

public class RedisManager {

	private RedisHelper numberIndex = null;

	private RedisHelper groupInfo = null;
	private RedisHelper token = null;
	private RedisHelper belong = null;
	private RedisHelper lastAgent = null;

	private static RedisManager m = null;

	private RedisManager() {

	}

	public static RedisManager getInstance() {
		if (m == null) {
			synchronized ("x01") {
				if (m == null) {
					m = new RedisManager();
					m.init();
				}
			}
		}
		return m;
	}

	public void init() {
		SerializableUtils.register(TokenEntity.class);
		SerializableUtils.register(Belong.class);
		if (StringUtils.isNotBlank(AgentEnv.getRedisHost())) {
			numberIndex = new RedisHelper(AgentEnv.getRedisHost(), AgentEnv.getRedisPort(), 1);
			token = new RedisHelper(AgentEnv.getRedisHost(), AgentEnv.getRedisPort(), 2);
			belong = new RedisHelper(AgentEnv.getRedisHost(), AgentEnv.getRedisPort(), 3);
			groupInfo = new RedisHelper(AgentEnv.getRedisHost(), AgentEnv.getRedisPort(), 4);
			lastAgent = new RedisHelper(AgentEnv.getRedisHost(), AgentEnv.getRedisPort(), 5);
		}
	}

	public String getQuestionNo(String pid) {
		return String.valueOf(numberIndex.incr("question_" + pid));
	}

	public String getCrmNo(String org) {
		return String.valueOf(numberIndex.incr("crm_" + org));
	}
	
	public String getOrderNo(String pid) {
		return String.valueOf(numberIndex.incr("order_" + pid));
	}

	public Integer getButtonNo(String pid) {
		return numberIndex.incr("button_" + pid).intValue();
	}

	public RedisHelper getBelong() {
		return belong;
	}

	public RedisHelper getToken() {
		return token;
	}

	public RedisHelper getLastAgent() {
		return lastAgent;
	}

	public RedisHelper getGroupInfo() {
		return groupInfo;
	}

}
