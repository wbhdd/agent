package cn.cellcom.agent.my.tag;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.form.FormUtil.Input;

/**
 * button<br/>
 * file<br/>
 * hidden <br/>
 * image<br/>
 * password <br/>
 * reset <br/>
 * submit<br/>
 * text<br/>
 * ------<br/>
 * ajax<br/>
 * 
 * @author zhengyj
 */
public class MyInput extends TagSupport {

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			out.write(gene());
		} catch (Exception e) {
			log.error("操作出现错误", e);
		}
		return 0;
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public String gene() {
		init();
		if (showonly) {
			if (max > 0) {// 指定了显示的文字长度
				return CU.subString(value, max, StringUtils.isNotBlank("……"));
			}
			return this.value;
		} else if ("ajax".equals(type)) {
			type = "button";
			StringBuffer sb = new StringBuffer(input(" onclick=\"ajaxSubmit('" + this.form + "');\""));
			return sb.toString();
		} else if ("date".equals(type)) {
			type = "text";
			StringBuffer sb = new StringBuffer(input(" onfocus=\"WdatePicker({dateFmt:'yyyy-MM-dd'})\""));
			return sb.toString();
		} else if ("datetime".equals(type) || "time".equals(type)) {
			type = "text";
			StringBuffer sb = new StringBuffer(input(" onfocus=\"WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})\""));
			return sb.toString();
		} else if ("file".equals(type) || "upload".equals(type)) {
			type = "file";
			StringBuffer sb = new StringBuffer(input(" onchange=\"uploadFile('" + this.token + "','" + this.url + "', '" + this.form + "')\""));
			return sb.toString();
		} else {
			return input(null);
		}
	}

	private void init() {
		if (StringUtils.isBlank(value)) {
			HttpSession session = (HttpSession) pageContext.getSession();
			List<Input> inputs = (List<Input>) session.getAttribute("PG-" + pageContext.getRequest().getParameter("entityName"));
			if (inputs != null) {
				for (int i = 0; i < inputs.size(); i++) {
					Input input = inputs.get(i);
					if (input.getName().equals(this.name)) {
						this.value = String.valueOf(input.getValue());
						break;
					}
				}
			}
		}
	}

	private String input(String something) {
		StringBuffer sb = new StringBuffer();
		sb.append("<input");
		sb.append(" type='").append(this.type).append("'");
		if (StringUtils.isNotBlank(id)) {
			sb.append(" id='").append(this.id).append("'");
		}
		sb.append(" name='").append(this.name).append("'");
		sb.append(" value='").append(this.value).append("'");
		if (StringUtils.isNotBlank(require)) {
			sb.append(" require='").append(this.require).append("'");
		}
		if (readonly == true) {
			sb.append(" readonly='").append(this.readonly).append("'");
		}
		if (StringUtils.isNotBlank(css)) {
			sb.append(" class='").append(this.css).append("'");
		}
		if (width > 0) {
			sb.append(" width='").append(width).append("px'");
		}
		if (height > 0) {
			sb.append(" height='").append(height).append("px'");
		}
		if (StringUtils.isNotBlank(something)) {
			sb.append(something);
		}
		sb.append(this.added);
		sb.append("/>");
		return sb.toString();
	}

	private boolean readonly = false;// 只显示
	private int max = 0;// 仅显示多少长度
	private boolean showonly = false;
	private String id;
	private String name;
	private String value = "";
	private String type = "";
	private String added = "";
	private String form = "";
	private String require;
	private String url = "";// 上传文件的url
	private String token = "";// 上传文件的token

	private String css;//指定样式

	private int width;//指定宽度

	private int height = 30;//指定高度
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public String getRequire() {
		return require;
	}

	public void setRequire(String require) {
		this.require = require;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

	public boolean isShowonly() {
		return showonly;
	}

	public void setShowonly(boolean showonly) {
		this.showonly = showonly;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAdded() {
		return added;
	}

	public void setAdded(String added) {
		this.added = added;
	}

}
