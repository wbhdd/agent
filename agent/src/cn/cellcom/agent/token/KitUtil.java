package cn.cellcom.agent.token;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

public class KitUtil {
	public static HashMap<String, String> parse(String authHeader) {
		HashMap<String, String> headerMap = null;
		String header = authHeader.substring(authHeader.lastIndexOf("Digest") + "Digest ".length());
		if (!StringUtils.isBlank(header)) {
			headerMap = new HashMap<String, String>();
			String[] params = header.split(",");
			if (params != null && params.length > 0) {
				for (String param : params) {

					String[] data = param.split("=");
					String key = data[0].trim();
					String value = "";

					if (data.length > 1) {
						value = data[1].trim();
					}

					headerMap.put(key, value);
				}
			}
		}
		return headerMap;
	}
}
