package cn.cellcom.agent.biz;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TOrg;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.MyException;

public class Org00Biz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {

		return null;
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}

	public TOrg getByPid(String pid) throws MyException {
		String hql = "from " + TOrg.class.getSimpleName() + " where pid=?";
		return (TOrg) this.getDao().myGet(hql, new String[] { pid });
	}
}
