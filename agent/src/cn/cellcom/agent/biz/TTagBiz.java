package cn.cellcom.agent.biz;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.agent.pojo.TTag;
import cn.cellcom.agent.struts.form.TTagForm;
import cn.cellcom.jar.biz.AbstractBiz;

public class TTagBiz extends AbstractBiz {
	
	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {
					
		TTagForm fm = (TTagForm) form;
		try {
			LogonSession ls = new LogonSession(req);
			log.info("delete log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}			
		String[] ids = fm.getIds();
		try {
			List list = new ArrayList();
			
						for (int i = 0; ids != null && i < ids.length; i++) {
				java.lang.String key = ids[i];
								
				list.add(this.getDao().myDelete(TTag.class, key));
				
							}
			
						
			return list;
		}catch (MyException e) {
			LogUtil.e(this.getClass(), "删除数据出现错误", e);
			return null;
		}
			}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
					return null;
			}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
					return null;
			
	}
}


