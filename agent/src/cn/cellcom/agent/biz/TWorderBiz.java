package cn.cellcom.agent.biz;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.pojo.TWorder;
import cn.cellcom.agent.struts.form.TWorderForm;
import cn.cellcom.agent.util.RedisManager;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.jar.util.IDGenerator;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TWorderBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {

		TWorderForm fm = (TWorderForm) form;
		try {
			LogonSession ls = new LogonSession(req);
			log.info("delete log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		String[] ids = fm.getIds();
		try {
			List list = new ArrayList();

			for (int i = 0; ids != null && i < ids.length; i++) {
				java.lang.String key = ids[i];

				list.add(this.getDao().myDelete(TWorder.class, key));

			}

			return list;
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "删除数据出现错误", e);
			return null;
		}
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		TWorderForm fm = (TWorderForm) form;
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();
		try {
			log.info("add log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			// 你必须修改这里的id值
			if (key != null && this.getDao().myGet(TWorder.class, key) != null) {
				return "该记录已经存在，不要重复添加";
			}
			TWorder pojo = new TWorder();
			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);
			pojo.setId(IDGenerator.getDefaultUUID());
			pojo.setOrg(user.getOrg());
			pojo.setPid(user.getPid());
			pojo.setCreateTime(System.currentTimeMillis());
			pojo.setSn(RedisManager.getInstance().getOrderNo(String.valueOf(ls.getAttribute("attribute.pid"))));
			if(StringUtils.isBlank(pojo.getCreateSource())) {
				pojo.setCreateSource(AgentConstant.CREATE_SOURCE.ADD.name());
			}
			pojo.setFagent(user.getUsername());
			this.getDao().mySave(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "新增数据出现错误", e);
		}
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		TWorderForm fm = (TWorderForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("modify log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			TWorder pojo = (TWorder) this.getDao().myGet(TWorder.class, key);
			if (pojo == null) {
				return "修改的记录不存在";
			}
			log.info("被修改的pojo：" + ToString.toString(TWorder.class, pojo, false));

			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);

			this.getDao().myUpdate(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "修改数据出现错误", e);
		}
	}
}
