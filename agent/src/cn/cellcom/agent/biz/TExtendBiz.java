package cn.cellcom.agent.biz;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.IDGenerator;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.agent.common.AgentConstant.EXTEND;
import cn.cellcom.agent.pojo.TExtend;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TExtendForm;
import cn.cellcom.jar.biz.AbstractBiz;

public class TExtendBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {

		return null;
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		TExtendForm fm = (TExtendForm) form;
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();
		try {
			log.info("add log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			// 你必须修改这里的id值
			if (key != null && this.getDao().myGet(TExtend.class, key) != null) {
				return "该记录已经存在，不要重复添加";
			}
			TExtend pojo = new TExtend();
			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);
			pojo.setOrg(user.getOrg());
			pojo.setPid(user.getPid());
			pojo.setCreateTime(System.currentTimeMillis());
			pojo.setId(IDGenerator.getDefaultUUID());
			this.getDao().mySave(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "新增数据出现错误", e);
		}
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		TExtendForm fm = (TExtendForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("modify log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			TExtend pojo = (TExtend) this.getDao().myGet(TExtend.class, key);
			if (pojo == null) {
				return "修改的记录不存在";
			}
			log.info("被修改的pojo：" + ToString.toString(TExtend.class, pojo, false));

			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);
			this.getDao().myUpdate(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "修改数据出现错误", e);
		}

	}

	/**
	 * 查询企业的某种扩展模块
	 * @param type
	 * @param pid
	 * @return
	 * @throws MyException
	 */
	public List getExtendList(String pid, EXTEND type) throws MyException {
		String hql = "from " + TExtend.class.getSimpleName() + " where type=? and pid=?";
		return this.getDao().myList(hql, new Object[] { String.valueOf(type.ordinal()), pid });
	}
}
