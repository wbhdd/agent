package cn.cellcom.agent.biz;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TQuestion;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TQuestionForm;
import cn.cellcom.agent.util.RedisManager;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.IDGenerator;
import cn.cellcom.jar.util.JsoupUtil;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TQuestionBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {
		TQuestionForm fm = (TQuestionForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("delete log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		String[] ids = fm.getIds();
		try {
			List list = new ArrayList();
			for (int i = 0; ids != null && i < ids.length; i++) {
				java.lang.String key = ids[i];
				TQuestion oneQuestion = (TQuestion) this.getDao().myDelete(TQuestion.class, key);
				if (oneQuestion != null) {
					// 已经被保存的图片
					List<String> imgList = JsoupUtil.getImgList(oneQuestion.getAnswer());
					imgList.addAll(JsoupUtil.getAhreList(oneQuestion.getAnswer()));
					for (int j = 0; j < imgList.size(); j++) {
						String oneImg = imgList.get(j);
						String file = oneImg.substring(oneImg.indexOf("fileName=") + "fileName=".length());
						FileLoadBiz.destoryTheQuestionFile(file, (TUser) ls.getLogonObject());
					}
				}
				list.add(oneQuestion);
			}

			return list;
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "删除数据出现错误", e);
			return null;
		}
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		TQuestionForm fm = (TQuestionForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("add log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			// 你必须修改这里的id值
			if (key != null && this.getDao().myGet(TQuestion.class, key) != null) {
				return "该记录已经存在，不要重复添加";
			}
			TQuestion pojo = new TQuestion();
			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);
			if (JsoupUtil.getText(pojo.getAnswer()).length() > 1000) {
				return "输入的内容过长";
			}
			TUser user = (TUser) ls.getLogonObject();
			pojo.setId(IDGenerator.getDefaultUUID());
			pojo.setOrg(user.getOrg());
			pojo.setPid(user.getPid());
			pojo.setNo(String.valueOf(RedisManager.getInstance().getQuestionNo(user.getPid())));
			pojo.setInsertTime(DT.getYYYYMMDDHHMMSS());
			pojo.setTimes(0);
			this.getDao().mySave(pojo);

			// 已经被保存的图片
			List<String> imgList = JsoupUtil.getImgList(pojo.getAnswer());
			imgList.addAll(JsoupUtil.getAhreList(pojo.getAnswer()));
			for (int i = 0; i < imgList.size(); i++) {
				String one = imgList.get(i);
				String file = one.substring(one.indexOf("fileName=") + "fileName=".length());
				FileLoadBiz.confirmTheQuestionFile(req, file);
			}
			
			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "新增数据出现错误", e);
		}
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		TQuestionForm fm = (TQuestionForm) form;
		LogonSession ls = new LogonSession(req);
		try {
			log.info("modify log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			TQuestion pojo = (TQuestion) this.getDao().myGet(TQuestion.class, key);
			if (pojo == null) {
				return "修改的记录不存在";
			}
			log.info("被修改的pojo：" + ToString.toString(TQuestion.class, pojo, false));
			List<String> oldImgList = JsoupUtil.getImgList(pojo.getAnswer());
			oldImgList.addAll(JsoupUtil.getAhreList(pojo.getAnswer()));

			CopyUtil.copy(req, pojo, null, false);
			if (JsoupUtil.getText(pojo.getAnswer()).length() > 1000) {
				return "输入的内容过长";
			}
			this.getDao().myUpdate(pojo);

			// 已经被保存的图片
			List<String> imgList = JsoupUtil.getImgList(pojo.getAnswer());
			imgList.addAll(JsoupUtil.getAhreList(pojo.getAnswer()));
			for (int i = 0; i < imgList.size(); i++) {
				String one = imgList.get(i);
				String file = one.substring(one.indexOf("fileName=") + "fileName=".length());
				FileLoadBiz.confirmTheQuestionFile(req, file);
			}
			// 旧的文件,如果没有被引用则要删掉了
			for (int i = 0; i < oldImgList.size(); i++) {
				boolean exist = false;
				String old = oldImgList.get(i);
				for (int j = 0; j < imgList.size(); j++) {
					if (imgList.get(j).equals(old)) {
						exist = true;
						break;
					}
				}
				if (!exist) {
					String file = old.substring(old.indexOf("fileName=") + "fileName=".length());
					FileLoadBiz.destoryTheQuestionFile(file, (TUser) ls.getLogonObject());
				}
			}

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "修改数据出现错误", e);
		}
	}

	/**
	 * 
	 * @param org
	 * @param number
	 * @return
	 * @throws MyException
	 */
	public List<TQuestion> getQuestionList(String pid, Object[] number) throws MyException {
		String hql = "from cn.cellcom.agent.pojo.TQuestion where (pid = ?) and (no in ?)";
		return (List<TQuestion>) this.getDao().myList(hql, new Object[] { pid, number });
	}

	/**
	 * 
	 * @param questionId
	 * @return
	 * @throws MyException
	 */
	public TQuestion getQuestion(String questionId) throws MyException {
		return (TQuestion) this.getDao().myGet(TQuestion.class, questionId);

	}

	/**
	 * 根据文本查找
	 * 
	 * @param text
	 * @return
	 * @throws MyException
	 */
	public List<TQuestion> getQuestionList(String pid, String text) throws MyException {
		String hql = "from cn.cellcom.agent.pojo.TQuestion where (pid = ?) and (question like ?)";
		return (List<TQuestion>) this.getDao().myList(hql, new Object[] { pid, text });
	}

	/**
	 * 更新对象
	 * 
	 * @param pojo
	 * @throws MyException
	 */
	public void modify(TQuestion pojo) throws MyException {
		this.getDao().myUpdate(pojo);
	}
}
