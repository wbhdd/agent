package cn.cellcom.agent.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.jar.util.DT;

public class TSettingEntity {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public static String TAG_SERVICE_TIME = "SERVICE_TIME";
	public static String TAG_TIP = "TIP";
	public static String TAG_ENTER = "ENTER";
	public static String TAG_SELF = "SELF";
	public static String TAG_LIMIT = "LIMIT";
	public static String TAG_AGENT = "AGENT";
	public static String TAG_SATISFY = "SATISFY";
	public static String TAG_FOR_AGENT = "FOR_AGENT";
	public static String TAG_EXTEND = "EXTEND";
	public static String TAG_UI = "UI";

	public static Map<String, List<String>> TAGS = new HashMap<String, List<String>>();

	// ????
	public static final String ROBOT_TYPE = "robotType";// ??????????
	public static final String TULING_KEY = "tulingKey";// ??????????
	public static final String WINDOW_HEIGHT = "windowHeight";// ??????
	public static final String WINDOW_WIDTH = "windowWidth";// ??????
	public static final String WINDOW_BACKGROUND_COLOR = "windowBackgroundColor";// ???????
	public static final String WINDOW_OPEN_TYPE = "windowOpenType";// ???????
	public static final String WINDOW_SHOW_TYPE = "windowShowType";// ?????????
	public static final String CALL_METHOD = "callMethod";// ???????
	public static final String FIRST_MENU_TAG = "firstMenuTag";// ???????
	public static final String KEY_WORD_TO_MANUAL = "keyWordToManual";// ?????????
	public static final String SELF_AUTO_TO_MANUAL_TIP = "selfAutoToManualTip";// ???????????
	public static final String SELF_ERROR_LIMIT = "selfErrorLimit";// ????X??????
	public static final String AGENT_TALKING_POLICY = "agentTalkingPolicy";// ?????????????
	public static final String GUIDE_POP_LATER = "guidePopLater";// ??????
	public static final String GUIDE_BUTTON_TEXT = "guideButtonText";// ??????
	public static final String AGENT_LOGOUT = "agentLogout";// ??????????????
	public static final String CALL_URL = "callUrl";// ????????
	public static final String ACCESS_KEY = "accessKey";// ACCESS_KEY
	public static final String LOGIN_DEFAULT_STATUS = "loginDefaultStatus";// ?????????
	public static final String BUTTON_TAG = "buttonTag";// ??????
	public static final String HISTORY_MESSAGE_POLICY = "historyMessagePolicy";// ?????????
	public static final String BEFORE_GROUP_LIST = "beforeGroupList";// ???????
	public static final String SELF_NAME = "selfName";// ??????????
	public static final String SELF_UNKNOW_TIP = "selfUnknowTip";// ???????
	public static final String SELF_SEND_QUESTION = "selfSendQuestion";// ????????
	public static final String QUEUE_TALKING_POLICY = "queueTalkingPolicy";// ?????????
	public static final String VISITOR_QUEUE_TIP = "visitorQueueTip";// ????????
	public static final String NO_ONLINE_AGENT_POLICY = "noOnlineAgentPolicy";// ?????????
	public static final String VISITOR_END_SESSION_TIP = "visitorEndSessionTip";// ????????
	public static final String SUPPORT_HOLDER = "supportHolder";// ????????
	public static final String SUPPORT_URL = "supportUrl";// ????????
	public static final String NO_AGENT_ONLINE_TIP = "noAgentOnlineTip";// ??????????
	public static final String ORG_HEADER = "orgHeader";// ??????
	public static final String SATISFY_LIMIT = "satisfyLimit";// ?????????
	public static final String SATISFY_INVITE_TYPE = "satisfyInviteType";// ????????
	public static final String SATISFY_THANKS_TIP = "satisfyThanksTip";// ???????
	public static final String SATISFY_INVITE_TIP = "satisfyInviteTip";// ????????
	public static final String SATISFY_INPUT = "satisfyInput";// ??????????????

	public static final String CHAT_MAX_LIMIT = "chatMaxLimit";// ????????
	public static final String CHAT_MAX_TYPE = "chatMaxType";// ??????????
	public static final String ENTER_FIRST_TIP = "enterFirstTip";// ????????
	public static final String SILENT_END_TIP = "silentEndTip";// ???????
	public static final String SILENT_NOTICE_TIP = "silentNoticeTip";// ??????
	public static final String SILENT_END_LIMIT = "silentEndLimit";// ???????
	public static final String SILENT_NOTICE_LIMIT = "silentNoticeLimit";// ???????
	public static final String QUEUE_LIMIT = "queueLimit";// ??????
	public static final String GUIDE_POP_TIP = "guidePopTip";// ????????
	public static final String SELF_POLICY = "selfPolicy";// ????????
	public static final String GUIDE_POLICY = "guidePolicy";// ????????
	public static final String VISITOR_QUEUE_POLICY = "visitorQueuePolicy";// ??????
	public static final String SESSION_DISPATCH_POLICY = "sessionDispatchPolicy";// ?????????
	/**
	 * ???????
	 */
	public static final String SERVICE_SETTING_MODE = "serviceSettingMode";
	public static final String SERVICE_DAY = "serviceDay";
	public static final String SERVICE_TIME = "serviceTime";
	public static final String OFFDUTY_REJECT = "offdutyReject";

	/**
	 * ?????
	 */
	public static final String OFFDUTY_REJECT_TIP = "offdutyRejectTip";
	public static final String OFFDUTY_RECEIPT_TIP = "offdutyReceiptTip";
	public static final String ONDUTY_RECEIPT_TIP = "ondutyReceiptTip";
	public static final String AGENT_END_SESSION_TIP = "agentEndSessionTip";
	public static final String OVERTIME_END_SESSION_TIP = "overtimeEndSessionTip";
	public static final String AGENT_LEFT_TIP = "agentLeftTip";

	static {
		/**
		 * ???????
		 */
		List<String> ondutyList = new ArrayList<String>();
		ondutyList.add(SERVICE_SETTING_MODE);
		ondutyList.add(SERVICE_DAY);
		ondutyList.add(SERVICE_TIME);
		ondutyList.add(OFFDUTY_REJECT);
		ondutyList.add(OFFDUTY_REJECT_TIP);
		ondutyList.add(OFFDUTY_RECEIPT_TIP);
		TAGS.put(TAG_SERVICE_TIME, ondutyList);

		/**
		 * ?????
		 */
		List<String> tipList = new ArrayList<String>();
		tipList.add(SELF_UNKNOW_TIP);
		tipList.add(VISITOR_QUEUE_TIP);
		tipList.add(VISITOR_END_SESSION_TIP);
		tipList.add(NO_AGENT_ONLINE_TIP);
		tipList.add(SATISFY_THANKS_TIP);
		tipList.add(SATISFY_INVITE_TIP);
		tipList.add(ENTER_FIRST_TIP);
		tipList.add(SILENT_END_TIP);
		tipList.add(SILENT_NOTICE_TIP);
		tipList.add(GUIDE_POP_TIP);
		tipList.add(OFFDUTY_REJECT_TIP);
		tipList.add(OFFDUTY_RECEIPT_TIP);
		tipList.add(ONDUTY_RECEIPT_TIP);
		tipList.add(AGENT_END_SESSION_TIP);
		tipList.add(OVERTIME_END_SESSION_TIP);
		tipList.add(AGENT_LEFT_TIP);
		TAGS.put(TAG_TIP, tipList);

		/**
		 * ????????
		 */
		List<String> enterList = new ArrayList<String>();
		enterList.add(GUIDE_POP_LATER);
		tipList.add(VISITOR_QUEUE_TIP);
		enterList.add(QUEUE_TALKING_POLICY);
		enterList.add(NO_ONLINE_AGENT_POLICY);
		// enterList.add(ORG_HEADER);
		enterList.add(ENTER_FIRST_TIP);
		enterList.add(QUEUE_LIMIT);
		enterList.add(GUIDE_POP_TIP);
		enterList.add(SELF_POLICY);
		enterList.add(GUIDE_POLICY);
		enterList.add(VISITOR_QUEUE_POLICY);
		enterList.add(SESSION_DISPATCH_POLICY);
		enterList.add(BEFORE_GROUP_LIST);
		enterList.add(HISTORY_MESSAGE_POLICY);
		TAGS.put(TAG_ENTER, enterList);

		List<String> limitList = new ArrayList<String>();
		limitList.add(SILENT_END_TIP);
		limitList.add(SILENT_NOTICE_TIP);
		limitList.add(SILENT_END_LIMIT);
		limitList.add(SILENT_NOTICE_LIMIT);
		limitList.add(QUEUE_LIMIT);
		TAGS.put(TAG_LIMIT, limitList);

		List<String> agentList = new ArrayList<String>();
		agentList.add(AGENT_TALKING_POLICY);
		agentList.add(AGENT_LOGOUT);
		agentList.add(LOGIN_DEFAULT_STATUS);
		agentList.add(CHAT_MAX_LIMIT);
		agentList.add(CHAT_MAX_TYPE);
		agentList.add(SESSION_DISPATCH_POLICY);
		TAGS.put(TAG_AGENT, agentList);

		List<String> satisfyList = new ArrayList<String>();
		satisfyList.add(SATISFY_LIMIT);
		satisfyList.add(SATISFY_INVITE_TYPE);
		satisfyList.add(SATISFY_THANKS_TIP);
		satisfyList.add(SATISFY_INVITE_TIP);
		satisfyList.add(SATISFY_INPUT);
		TAGS.put(TAG_SATISFY, satisfyList);

		List<String> forAgentList = new ArrayList<String>();
		forAgentList.add(CHAT_MAX_LIMIT);
		TAGS.put(TAG_FOR_AGENT, forAgentList);

		List<String> selfList = new ArrayList<String>();
selfList.add(ROBOT_TYPE);
		selfList.add(TULING_KEY);
		selfList.add(ROBOT_TYPE);
		selfList.add(KEY_WORD_TO_MANUAL);
		selfList.add(SELF_AUTO_TO_MANUAL_TIP);
		selfList.add(SELF_ERROR_LIMIT);
		selfList.add(SELF_NAME);
		selfList.add(SELF_UNKNOW_TIP);
		selfList.add(SELF_SEND_QUESTION);
		selfList.add(SELF_POLICY);
		selfList.add(ENTER_FIRST_TIP);
		TAGS.put(TAG_SELF, selfList);

		List<String> extendList = new ArrayList<String>();
		extendList.add(CALL_METHOD);
		extendList.add(CALL_URL);
		extendList.add(ACCESS_KEY);
		TAGS.put(TAG_EXTEND, extendList);

		List<String> uiList = new ArrayList<String>();
		uiList.add(WINDOW_HEIGHT);
		uiList.add(WINDOW_WIDTH);
		uiList.add(WINDOW_BACKGROUND_COLOR);
		uiList.add(WINDOW_OPEN_TYPE);
		uiList.add(WINDOW_SHOW_TYPE);
		uiList.add(FIRST_MENU_TAG);
		uiList.add(SUPPORT_HOLDER);
		uiList.add(SUPPORT_URL);
		uiList.add(BUTTON_TAG);
		uiList.add(GUIDE_BUTTON_TEXT);
		TAGS.put(TAG_UI, uiList);
	}

	// ?????????????
	/**
	 * ??????
	 */
	private Integer windowHeight;
	/**
	 * ??????
	 */
	private Integer windowWidth;
	/**
	 * ???????
	 */
	private String windowBackgroundColor;
	/**
	 * ???????
	 */
	private String windowOpenType;
	/**
	 * ?????????
	 */
	private String windowShowType;
	/**
	 * ???????
	 */
	private String callMethod;
	/**
	 * ???????
	 */
	private String firstMenuTag;
	/**
	 * ?????????
	 */
	private String keyWordToManual = "????";
	/**
	 * ???????????
	 */
	private String selfAutoToManualTip;
	/**
	 * ????X??????
	 */
	private Integer selfErrorLimit;
	/**
	 * ?????????????
	 */
	private Integer agentTalkingPolicy;
	/**
	 * ??????
	 */
	private Integer guidePopLater;
	/**
	 * ??????
	 */
	private String guideButtonText;
	/**
	 * ??????????????
	 */
	private Integer agentLogout;
	/**
	 * ????????
	 */
	private String callUrl;
	/**
	 * ACCESS_KEY
	 */
	private String accessKey;
	/**
	 * ?????????
	 */
	private String loginDefaultStatus;
	/**
	 * ??????
	 */
	private String buttonTag;

	/**
	 * ?????????
	 */
	private Integer historyMessagePolicy;
	/**
	 * ???????
	 */
	private String beforeGroupList;
	/**
	 * ??????????
	 */
	private String selfName;
	/**
	 * ???????
	 */
	private String selfUnknowTip;
	/**
	 * ????????
	 */
	private String selfSendQuestion;
	/**
	 * ?????????
	 */
	private Integer queueTalkingPolicy;
	/**
	 * ????????
	 */
	private String visitorQueueTip;
	/**
	 * ?????????
	 */
	private Integer noOnlineAgentPolicy;
	/**
	 * ????????
	 */
	private String visitorEndSessionTip;
	/**
	 * ???????
	 */
	private String supportHolder;
	/**
	 * ????????
	 */
	private String supportUrl;
	/**
	 * ??????????
	 */
	private String noAgentOnlineTip;
	/**
	 * ??????
	 */
	private String orgHeader;
	/**
	 * ?????????
	 */
	private Integer satisfyLimit;
	/**
	 * ????????
	 */
	private Integer satisfyInviteType;
	/**
	 * ???????
	 */
	private String satisfyThanksTip;
	/**
	 * ????????
	 */
	private String satisfyInviteTip;
	/**
	 * ????????
	 */
	private Integer chatMaxLimit;
	/**
	 * ??????????
	 */
	private Integer chatMaxType;
	/**
	 * ????????
	 */
	private String enterFirstTip;
	/**
	 * ???????
	 */
	private String silentEndTip;
	/**
	 * ??????
	 */
	private String silentNoticeTip;
	/**
	 * ???????
	 */
	private Integer silentEndLimit;
	/**
	 * ???????
	 */
	private Integer silentNoticeLimit;
	/**
	 * ??????
	 */
	private Integer queueLimit;
	/**
	 * ????????
	 */
	private String guidePopTip;
	/**
	 * ????????
	 */
	private Integer selfPolicy;
	/**
	 * ????????
	 */
	private Integer guidePolicy;
	/**
	 * ??????
	 */
	private Integer visitorQueuePolicy;
	/**
	 * ?????????
	 */
	private Integer sessionDispatchPolicy;

	/**
	 * ??????????????0??????????????????????? 1?????????????????????????????
	 */
	private Integer serviceSettingMode;

	/**
	 * 1,2,3,4,5????????????????????
	 */
	private String serviceDay;

	/**
	 * ?????????????
	 * 08:00-12:00,13:00-18:00;08:00-12:00,13:00-18:00;08:00-12:00,13:00-18:00;08:00-12:00,13:00-18:00;
	 */
	private String serviceTime;

	/**
	 * ????????????
	 */
	private String offdutyReject;

	/**
	 * ??????????????
	 */
	private String offdutyRejectTip;

	/**
	 * ????????????
	 */
	private String tipSettingMode;

	/**
	 * ???????????????
	 */
	private String offdutyReceiptTip;

	/**
	 * ??????????????
	 */
	private String ondutyReceiptTip;

	/**
	 * ????????????????
	 */
	private String agentEndSessionTip;

	/**
	 * ????????????????
	 */
	private String overtimeEndSessionTip;

	/**
	 * ?????????????
	 */
	private String agentLeftTip;

	/**
	 * ????????????
	 */
	private String tipSettingModeSwitch;

	/**
	 * 
	 */
	private String orgName;

	private Integer satisfyInput;

	private Integer robotType;

	private String tulingKey;

	// setter/getter
	public String getTulingKey() {
		return tulingKey;
	}

	public void setTulingKey(String tulingKey) {
		this.tulingKey = tulingKey;
	}

	public Integer getRobotType() {
		return robotType;
	}

	public void setRobotType(Integer robotType) {
		this.robotType = robotType;
	}

	public Integer getWindowHeight() {
		return windowHeight;
	}

	public void setWindowHeight(Integer windowHeight) {
		this.windowHeight = windowHeight;
	}

	public Integer getWindowWidth() {
		return windowWidth;
	}

	public void setWindowWidth(Integer windowWidth) {
		this.windowWidth = windowWidth;
	}

	public String getWindowBackgroundColor() {
		return windowBackgroundColor;
	}

	public void setWindowBackgroundColor(String windowBackgroundColor) {
		this.windowBackgroundColor = windowBackgroundColor;
	}

	public String getWindowOpenType() {
		return windowOpenType;
	}

	public void setWindowOpenType(String windowOpenType) {
		this.windowOpenType = windowOpenType;
	}

	public String getWindowShowType() {
		return windowShowType;
	}

	public void setWindowShowType(String windowShowType) {
		this.windowShowType = windowShowType;
	}

	public String getCallMethod() {
		return callMethod;
	}

	public void setCallMethod(String callMethod) {
		this.callMethod = callMethod;
	}

	public String getFirstMenuTag() {
		return firstMenuTag;
	}

	public void setFirstMenuTag(String firstMenuTag) {
		this.firstMenuTag = firstMenuTag;
	}

	public String getKeyWordToManual() {
		return keyWordToManual;
	}

	public void setKeyWordToManual(String keyWordToManual) {
		this.keyWordToManual = keyWordToManual;
	}

	public String getSelfAutoToManualTip() {
		return selfAutoToManualTip;
	}

	public void setSelfAutoToManualTip(String selfAutoToManualTip) {
		this.selfAutoToManualTip = selfAutoToManualTip;
	}

	public Integer getSelfErrorLimit() {
		return selfErrorLimit;
	}

	public void setSelfErrorLimit(Integer selfErrorLimit) {
		this.selfErrorLimit = selfErrorLimit;
	}

	public Integer getAgentTalkingPolicy() {
		return agentTalkingPolicy;
	}

	public void setAgentTalkingPolicy(Integer agentTalkingPolicy) {
		this.agentTalkingPolicy = agentTalkingPolicy;
	}

	public Integer getGuidePopLater() {
		return guidePopLater;
	}

	public void setGuidePopLater(Integer guidePopLater) {
		this.guidePopLater = guidePopLater;
	}

	public String getGuideButtonText() {
		return guideButtonText;
	}

	public void setGuideButtonText(String guideButtonText) {
		this.guideButtonText = guideButtonText;
	}

	public Integer getAgentLogout() {
		return agentLogout;
	}

	public void setAgentLogout(Integer agentLogout) {
		this.agentLogout = agentLogout;
	}

	public String getCallUrl() {
		return callUrl;
	}

	public void setCallUrl(String callUrl) {
		this.callUrl = callUrl;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getLoginDefaultStatus() {
		return loginDefaultStatus;
	}

	public void setLoginDefaultStatus(String loginDefaultStatus) {
		this.loginDefaultStatus = loginDefaultStatus;
	}

	public String getButtonTag() {
		return buttonTag;
	}

	public void setButtonTag(String buttonTag) {
		this.buttonTag = buttonTag;
	}

	public Integer getHistoryMessagePolicy() {
		return historyMessagePolicy;
	}

	public void setHistoryMessagePolicy(Integer historyMessagePolicy) {
		this.historyMessagePolicy = historyMessagePolicy;
	}

	public String getBeforeGroupList() {
		return beforeGroupList;
	}

	public void setBeforeGroupList(String beforeGroupList) {
		this.beforeGroupList = beforeGroupList;
	}

	public String getSelfName() {
		return selfName;
	}

	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}

	public String getSelfSendQuestion() {
		return selfSendQuestion;
	}

	public void setSelfSendQuestion(String selfSendQuestion) {
		this.selfSendQuestion = selfSendQuestion;
	}

	public Integer getQueueTalkingPolicy() {
		return queueTalkingPolicy;
	}

	public void setQueueTalkingPolicy(Integer queueTalkingPolicy) {
		this.queueTalkingPolicy = queueTalkingPolicy;
	}

	public String getVisitorQueueTip() {
		return visitorQueueTip;
	}

	public void setVisitorQueueTip(String visitorQueueTip) {
		this.visitorQueueTip = visitorQueueTip;
	}

	public Integer getNoOnlineAgentPolicy() {
		return noOnlineAgentPolicy;
	}

	public void setNoOnlineAgentPolicy(Integer noOnlineAgentPolicy) {
		this.noOnlineAgentPolicy = noOnlineAgentPolicy;
	}

	public String getVisitorEndSessionTip() {
		return visitorEndSessionTip;
	}

	public void setVisitorEndSessionTip(String visitorEndSessionTip) {
		this.visitorEndSessionTip = visitorEndSessionTip;
	}

	public String getSupportHolder() {
		return supportHolder;
	}

	public void setSupportHolder(String supportHolder) {
		this.supportHolder = supportHolder;
	}

	public String getSupportUrl() {
		return supportUrl;
	}

	public void setSupportUrl(String supportUrl) {
		this.supportUrl = supportUrl;
	}

	public String getNoAgentOnlineTip() {
		return noAgentOnlineTip;
	}

	public void setNoAgentOnlineTip(String noAgentOnlineTip) {
		this.noAgentOnlineTip = noAgentOnlineTip;
	}

	public String getOrgHeader() {
		return orgHeader;
	}

	public void setOrgHeader(String orgHeader) {
		this.orgHeader = orgHeader;
	}

	public Integer getSatisfyLimit() {
		return satisfyLimit;
	}

	public void setSatisfyLimit(Integer satisfyLimit) {
		this.satisfyLimit = satisfyLimit;
	}

	public Integer getSatisfyInviteType() {
		return satisfyInviteType;
	}

	public void setSatisfyInviteType(Integer satisfyInviteType) {
		this.satisfyInviteType = satisfyInviteType;
	}

	public String getSatisfyThanksTip() {
		return satisfyThanksTip;
	}

	public void setSatisfyThanksTip(String satisfyThanksTip) {
		this.satisfyThanksTip = satisfyThanksTip;
	}

	public String getSatisfyInviteTip() {
		return satisfyInviteTip;
	}

	public void setSatisfyInviteTip(String satisfyInviteTip) {
		this.satisfyInviteTip = satisfyInviteTip;
	}

	public Integer getChatMaxLimit() {
		return chatMaxLimit;
	}

	public void setChatMaxLimit(Integer chatMaxLimit) {
		this.chatMaxLimit = chatMaxLimit;
	}

	public Integer getChatMaxType() {
		return chatMaxType;
	}

	public void setChatMaxType(Integer chatMaxType) {
		this.chatMaxType = chatMaxType;
	}

	public String getEnterFirstTip() {
		return enterFirstTip;
	}

	public void setEnterFirstTip(String enterFirstTip) {
		this.enterFirstTip = enterFirstTip;
	}

	public String getSilentEndTip() {
		return silentEndTip;
	}

	public void setSilentEndTip(String silentEndTip) {
		this.silentEndTip = silentEndTip;
	}

	public String getSilentNoticeTip() {
		return silentNoticeTip;
	}

	public void setSilentNoticeTip(String silentNoticeTip) {
		this.silentNoticeTip = silentNoticeTip;
	}

	public Integer getSilentEndLimit() {
		return silentEndLimit;
	}

	public void setSilentEndLimit(Integer silentEndLimit) {
		this.silentEndLimit = silentEndLimit;
	}

	public Integer getSilentNoticeLimit() {
		return silentNoticeLimit;
	}

	public void setSilentNoticeLimit(Integer silentNoticeLimit) {
		this.silentNoticeLimit = silentNoticeLimit;
	}

	public Integer getQueueLimit() {
		return queueLimit;
	}

	public void setQueueLimit(Integer queueLimit) {
		this.queueLimit = queueLimit;
	}

	public String getGuidePopTip() {
		return guidePopTip;
	}

	public void setGuidePopTip(String guidePopTip) {
		this.guidePopTip = guidePopTip;
	}

	public Integer getSelfPolicy() {
		return selfPolicy;
	}

	public void setSelfPolicy(Integer selfPolicy) {
		this.selfPolicy = selfPolicy;
	}

	public Integer getGuidePolicy() {
		return guidePolicy;
	}

	public void setGuidePolicy(Integer guidePolicy) {
		this.guidePolicy = guidePolicy;
	}

	public Integer getVisitorQueuePolicy() {
		return visitorQueuePolicy;
	}

	public void setVisitorQueuePolicy(Integer visitorQueuePolicy) {
		this.visitorQueuePolicy = visitorQueuePolicy;
	}

	public Integer getSessionDispatchPolicy() {
		return sessionDispatchPolicy;
	}

	public void setSessionDispatchPolicy(Integer sessionDispatchPolicy) {
		this.sessionDispatchPolicy = sessionDispatchPolicy;
	}

	public Integer getServiceSettingMode() {
		return serviceSettingMode;
	}

	public void setServiceSettingMode(Integer serviceSettingMode) {
		this.serviceSettingMode = serviceSettingMode;
	}

	public String getServiceDay() {
		return serviceDay;
	}

	public void setServiceDay(String serviceDay) {
		this.serviceDay = serviceDay;
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getOffdutyReject() {
		return offdutyReject;
	}

	public void setOffdutyReject(String offdutyReject) {
		this.offdutyReject = offdutyReject;
	}

	public String getOffdutyRejectTip() {
		return offdutyRejectTip;
	}

	public void setOffdutyRejectTip(String offdutyRejectTip) {
		this.offdutyRejectTip = offdutyRejectTip;
	}

	public String getTipSettingMode() {
		return tipSettingMode;
	}

	public void setTipSettingMode(String tipSettingMode) {
		this.tipSettingMode = tipSettingMode;
	}

	public String getOffdutyReceiptTip() {
		return offdutyReceiptTip;
	}

	public void setOffdutyReceiptTip(String offdutyReceiptTip) {
		this.offdutyReceiptTip = offdutyReceiptTip;
	}

	public String getOndutyReceiptTip() {
		return ondutyReceiptTip;
	}

	public void setOndutyReceiptTip(String ondutyReceiptTip) {
		this.ondutyReceiptTip = ondutyReceiptTip;
	}

	public String getAgentEndSessionTip() {
		return agentEndSessionTip;
	}

	public void setAgentEndSessionTip(String agentEndSessionTip) {
		this.agentEndSessionTip = agentEndSessionTip;
	}

	public String getOvertimeEndSessionTip() {
		return overtimeEndSessionTip;
	}

	public void setOvertimeEndSessionTip(String overtimeEndSessionTip) {
		this.overtimeEndSessionTip = overtimeEndSessionTip;
	}

	public String getAgentLeftTip() {
		return agentLeftTip;
	}

	public void setAgentLeftTip(String agentLeftTip) {
		this.agentLeftTip = agentLeftTip;
	}

	public String getTipSettingModeSwitch() {
		return tipSettingModeSwitch;
	}

	public void setTipSettingModeSwitch(String tipSettingModeSwitch) {
		this.tipSettingModeSwitch = tipSettingModeSwitch;
	}

	public static List<String> getItemList(String tag) {
		return TAGS.get(tag);
	}

	public String getSelfUnknowTip() {
		return selfUnknowTip;
	}

	public void setSelfUnknowTip(String selfUnknowTip) {
		this.selfUnknowTip = selfUnknowTip;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String toString() {
		return "TSettingEntity [loginDefaultStatus=" + loginDefaultStatus + ", buttonTag=" + buttonTag + ", historyMessagePolicy="
				+ historyMessagePolicy + ", beforeGroupList=" + beforeGroupList + ", selfName=" + selfName + ", selfUnknowTip=" + selfUnknowTip
				+ ", selfSendQuestion=" + selfSendQuestion + ", queueTalkingPolicy=" + queueTalkingPolicy + ", visitorQueueTip=" + visitorQueueTip
				+ ", noOnlineAgentPolicy=" + noOnlineAgentPolicy + ", visitorEndSessionTip=" + visitorEndSessionTip + ", supportHolder="
				+ supportHolder + ", supportUrl=" + supportUrl + ", noAgentOnlineTip=" + noAgentOnlineTip + ", orgHeader=" + orgHeader
				+ ", satisfyLimit=" + satisfyLimit + ", satisfyInviteType=" + satisfyInviteType + ", satisfyThanksTip=" + satisfyThanksTip
				+ ", satisfyInviteTip=" + satisfyInviteTip + ", chatMaxLimit=" + chatMaxLimit + ", chatMaxType=" + chatMaxType + ", enterFirstTip="
				+ enterFirstTip + ", silentEndTip=" + silentEndTip + ", silentNoticeTip=" + silentNoticeTip + ", silentEndLimit=" + silentEndLimit
				+ ", silentNoticeLimit=" + silentNoticeLimit + ", queueLimit=" + queueLimit + ", guidePopTip=" + guidePopTip + ", selfPolicy="
				+ selfPolicy + ", guidePolicy=" + guidePolicy + ", visitorQueuePolicy=" + visitorQueuePolicy + ", sessionDispatchPolicy="
				+ sessionDispatchPolicy + ", serviceSettingMode=" + serviceSettingMode + ", serviceDay=" + serviceDay + ", serviceTime=" + serviceTime
				+ ", offdutyReject=" + offdutyReject + ", offdutyRejectTip=" + offdutyRejectTip + ", tipSettingMode=" + tipSettingMode
				+ ", offdutyReceiptTip=" + offdutyReceiptTip + ", ondutyReceiptTip=" + ondutyReceiptTip + ", agentEndSessionTip=" + agentEndSessionTip
				+ ", overtimeEndSessionTip=" + overtimeEndSessionTip + ", agentLeftTip=" + agentLeftTip + ", tipSettingModeSwitch="
				+ tipSettingModeSwitch + ", orgName=" + orgName + "]";
	}

	/**
	 * ?????????????
	 * 
	 * @return
	 */
	public boolean offdutyTime() {
		boolean offduty = false;
		int indx = this.getServiceDay().indexOf(String.valueOf(DT.getDayOfWeek(DT.getNow())));
		if (indx < 0) {// ????????????????
			offduty = true;
		} else {// ?????????????????????????
			String now = DT.getHHMM(DT.getNow());
			String am = this.getServiceTime().split(",")[0];
			String[] amArr = am.split("-");
			String pm = this.getServiceTime().split(",")[1];
			String[] pmArr = pm.split("-");
			// ?????????????
			if ((amArr[0].compareTo(now) <= 0 && amArr[1].compareTo(now) >= 0) || (pmArr[0].compareTo(now) <= 0 && pmArr[1].compareTo(now) >= 0)) {
				offduty = false;
			} else {// ?????????????
				offduty = true;
			}
		}
		log.info("[{}] check offduty time[{}], result is[{}]", this.getOrgName(), this.getServiceDay() + "//" + this.getServiceTime(), offduty);
		return offduty;
	}

	public Integer getSatisfyInput() {
		return satisfyInput;
	}

	public void setSatisfyInput(Integer satisfyInput) {
		this.satisfyInput = satisfyInput;
	}
}
