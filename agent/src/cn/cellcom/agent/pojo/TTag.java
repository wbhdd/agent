package cn.cellcom.agent.pojo;

/**
 * TTag entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TTag implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private Integer no;
	private String tagTxt;
	private String fatherId;

	// Constructors

	/** default constructor */
	public TTag() {
	}

	/** minimal constructor */
	public TTag(String id, String org, Integer no, String tagTxt) {
		this.id = id;
		this.org = org;
		this.no = no;
		this.tagTxt = tagTxt;
	}

	/** full constructor */
	public TTag(String id, String org, Integer no, String tagTxt, String fatherId) {
		this.id = id;
		this.org = org;
		this.no = no;
		this.tagTxt = tagTxt;
		this.fatherId = fatherId;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public Integer getNo() {
		return this.no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getTagTxt() {
		return this.tagTxt;
	}

	public void setTagTxt(String tagTxt) {
		this.tagTxt = tagTxt;
	}

	public String getFatherId() {
		return this.fatherId;
	}

	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}

}
