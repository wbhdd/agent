package cn.cellcom.agent.pojo;

import java.util.Date;

/**
 * TChannel entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TChannel implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String no;
	private String name;
	private String memo;
	private String type;
	private Date insertTime;
	private String wcAppid;
	private String wcSecret;
	private String webUrl;
	private String qq;

	// Constructors

	/** default constructor */
	public TChannel() {
	}

	/** minimal constructor */
	public TChannel(String id, String org, String pid, String no, String name, String type, Date insertTime) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.no = no;
		this.name = name;
		this.type = type;
		this.insertTime = insertTime;
	}

	/** full constructor */
	public TChannel(String id, String org, String pid, String no, String name, String memo, String type, Date insertTime, String wcAppid, String wcSecret, String webUrl) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.no = no;
		this.name = name;
		this.memo = memo;
		this.type = type;
		this.insertTime = insertTime;
		this.wcAppid = wcAppid;
		this.wcSecret = wcSecret;
		this.webUrl = webUrl;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getNo() {
		return this.no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getWcAppid() {
		return this.wcAppid;
	}

	public void setWcAppid(String wcAppid) {
		this.wcAppid = wcAppid;
	}

	public String getWcSecret() {
		return this.wcSecret;
	}

	public void setWcSecret(String wcSecret) {
		this.wcSecret = wcSecret;
	}

	public String getWebUrl() {
		return this.webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
}