package cn.cellcom.agent.pojo;

/**
 * TGroup entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TGroup implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String no;
	private String name;
	private String memo;
	private String tmp;

	// Constructors

	/** default constructor */
	public TGroup() {
	}

	/** minimal constructor */
	public TGroup(String id, String org, String pid, String no, String name) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.no = no;
		this.name = name;
	}

	/** full constructor */
	public TGroup(String id, String org, String pid, String no, String name, String memo) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.no = no;
		this.name = name;
		this.memo = memo;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getNo() {
		return this.no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getTmp() {
		return tmp;
	}

	public void setTmp(String tmp) {
		this.tmp = tmp;
	}

}
