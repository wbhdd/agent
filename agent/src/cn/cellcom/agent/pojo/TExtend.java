package cn.cellcom.agent.pojo;

/**
 * TExtend entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TExtend implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String name;
	private String type;
	private String url;
	private Long createTime;

	// Constructors

	/** default constructor */
	public TExtend() {
	}

	/** full constructor */
	public TExtend(String id, String org, String pid, String name, String type, String url, Long createTime) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.name = name;
		this.type = type;
		this.url = url;
		this.createTime = createTime;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

}