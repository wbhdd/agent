package cn.cellcom.agent.pojo;

/**
 * TLw entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TLw implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String sid;
	private String channel;
	private String name;
	private String phone;
	private String email;
	private String status;
	private String qq;
	private String memo;
	private String crm;
	private String handleUser;
	private Long handleTime;
	private String handleResult;
	private String tag1;
	private String tag2;
	private String tag3;
	private String tag4;
	private Long createTime;
	private String extend1;
	private String extend2;
	private String extend3;
	private String extend4;
	private String extend5;
	private String extend6;
	private String extend7;
	private String extend8;
	private String extend9;
	private String extend10;

	// Constructors

	/** default constructor */
	public TLw() {
	}

	/** minimal constructor */
	public TLw(String id, String org, String pid, String name, Long createTime) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.name = name;
		this.createTime = createTime;
	}

	/** full constructor */
	public TLw(String id, String org, String pid, String sid, String channel, String name, String phone, String email, String status, String qq,
			String memo, String crm, String handleUser, Long handleTime, String handleResult, String tag1, String tag2, String tag3, String tag4,
			Long createTime, String extend1, String extend2, String extend3, String extend4, String extend5, String extend6, String extend7,
			String extend8, String extend9, String extend10) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.sid = sid;
		this.channel = channel;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.status = status;
		this.qq = qq;
		this.memo = memo;
		this.crm = crm;
		this.handleUser = handleUser;
		this.handleTime = handleTime;
		this.handleResult = handleResult;
		this.tag1 = tag1;
		this.tag2 = tag2;
		this.tag3 = tag3;
		this.tag4 = tag4;
		this.createTime = createTime;
		this.extend1 = extend1;
		this.extend2 = extend2;
		this.extend3 = extend3;
		this.extend4 = extend4;
		this.extend5 = extend5;
		this.extend6 = extend6;
		this.extend7 = extend7;
		this.extend8 = extend8;
		this.extend9 = extend9;
		this.extend10 = extend10;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSid() {
		return this.sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQq() {
		return this.qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCrm() {
		return this.crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getHandleUser() {
		return this.handleUser;
	}

	public void setHandleUser(String handleUser) {
		this.handleUser = handleUser;
	}

	public Long getHandleTime() {
		return this.handleTime;
	}

	public void setHandleTime(Long handleTime) {
		this.handleTime = handleTime;
	}

	public String getHandleResult() {
		return this.handleResult;
	}

	public void setHandleResult(String handleResult) {
		this.handleResult = handleResult;
	}

	public String getTag1() {
		return this.tag1;
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public String getTag2() {
		return this.tag2;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public String getTag3() {
		return this.tag3;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}

	public String getTag4() {
		return this.tag4;
	}

	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getExtend1() {
		return this.extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}

	public String getExtend2() {
		return this.extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}

	public String getExtend3() {
		return this.extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}

	public String getExtend4() {
		return this.extend4;
	}

	public void setExtend4(String extend4) {
		this.extend4 = extend4;
	}

	public String getExtend5() {
		return this.extend5;
	}

	public void setExtend5(String extend5) {
		this.extend5 = extend5;
	}

	public String getExtend6() {
		return this.extend6;
	}

	public void setExtend6(String extend6) {
		this.extend6 = extend6;
	}

	public String getExtend7() {
		return this.extend7;
	}

	public void setExtend7(String extend7) {
		this.extend7 = extend7;
	}

	public String getExtend8() {
		return this.extend8;
	}

	public void setExtend8(String extend8) {
		this.extend8 = extend8;
	}

	public String getExtend9() {
		return this.extend9;
	}

	public void setExtend9(String extend9) {
		this.extend9 = extend9;
	}

	public String getExtend10() {
		return this.extend10;
	}

	public void setExtend10(String extend10) {
		this.extend10 = extend10;
	}

}