package cn.cellcom.agent.pojo;

/**
 * TWorder entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TWorder implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String sn;
	private String status;
	private Integer level;
	private String crm;
	private String fagent;
	private String toAgent;
	private String subject;
	private String context;
	private String createSource;
	private String createObject;
	private Long createTime;
	private String tag1;
	private String tag2;
	private String tag3;
	private String tag4;
	private String extend1;
	private String extend2;
	private String extend3;
	private String extend4;
	private String extend5;
	private String extend6;
	private String extend7;
	private String extend8;
	private String extend9;
	private String extend10;

	// Constructors

	/** default constructor */
	public TWorder() {
	}

	/** minimal constructor */
	public TWorder(String id, String org, String pid, String sn, String status, String crm, Long createTime) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.sn = sn;
		this.status = status;
		this.crm = crm;
		this.createTime = createTime;
	}

	/** full constructor */
	public TWorder(String id, String org, String pid, String sn, String status, Integer level, String crm, String fagent, String toAgent,
			String subject, String context, String createSource, String createObject, Long createTime, String tag1, String tag2, String tag3,
			String tag4, String extend1, String extend2, String extend3, String extend4, String extend5, String extend6, String extend7,
			String extend8, String extend9, String extend10) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.sn = sn;
		this.status = status;
		this.level = level;
		this.crm = crm;
		this.fagent = fagent;
		this.toAgent = toAgent;
		this.subject = subject;
		this.context = context;
		this.createSource = createSource;
		this.createObject = createObject;
		this.createTime = createTime;
		this.tag1 = tag1;
		this.tag2 = tag2;
		this.tag3 = tag3;
		this.tag4 = tag4;
		this.extend1 = extend1;
		this.extend2 = extend2;
		this.extend3 = extend3;
		this.extend4 = extend4;
		this.extend5 = extend5;
		this.extend6 = extend6;
		this.extend7 = extend7;
		this.extend8 = extend8;
		this.extend9 = extend9;
		this.extend10 = extend10;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSn() {
		return this.sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getCrm() {
		return this.crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getFagent() {
		return this.fagent;
	}

	public void setFagent(String fagent) {
		this.fagent = fagent;
	}

	public String getToAgent() {
		return this.toAgent;
	}

	public void setToAgent(String toAgent) {
		this.toAgent = toAgent;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContext() {
		return this.context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getCreateSource() {
		return this.createSource;
	}

	public void setCreateSource(String createSource) {
		this.createSource = createSource;
	}

	public String getCreateObject() {
		return this.createObject;
	}

	public void setCreateObject(String createObject) {
		this.createObject = createObject;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getTag1() {
		return this.tag1;
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}

	public String getTag2() {
		return this.tag2;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}

	public String getTag3() {
		return this.tag3;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}

	public String getTag4() {
		return this.tag4;
	}

	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}

	public String getExtend1() {
		return this.extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}

	public String getExtend2() {
		return this.extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}

	public String getExtend3() {
		return this.extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}

	public String getExtend4() {
		return this.extend4;
	}

	public void setExtend4(String extend4) {
		this.extend4 = extend4;
	}

	public String getExtend5() {
		return this.extend5;
	}

	public void setExtend5(String extend5) {
		this.extend5 = extend5;
	}

	public String getExtend6() {
		return this.extend6;
	}

	public void setExtend6(String extend6) {
		this.extend6 = extend6;
	}

	public String getExtend7() {
		return this.extend7;
	}

	public void setExtend7(String extend7) {
		this.extend7 = extend7;
	}

	public String getExtend8() {
		return this.extend8;
	}

	public void setExtend8(String extend8) {
		this.extend8 = extend8;
	}

	public String getExtend9() {
		return this.extend9;
	}

	public void setExtend9(String extend9) {
		this.extend9 = extend9;
	}

	public String getExtend10() {
		return this.extend10;
	}

	public void setExtend10(String extend10) {
		this.extend10 = extend10;
	}

}