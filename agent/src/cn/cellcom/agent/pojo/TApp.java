package cn.cellcom.agent.pojo;

import java.util.Date;

/**
 * TApp entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TApp implements java.io.Serializable {

	// Fields

	private String id;
	private Integer no;
	private String org;
	private String name;
	private Date insertTime;

	// Constructors

	/** default constructor */
	public TApp() {
	}

	/** full constructor */
	public TApp(String id, Integer no, String org, String name, Date insertTime) {
		this.id = id;
		this.no = no;
		this.org = org;
		this.name = name;
		this.insertTime = insertTime;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getNo() {
		return this.no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

}
