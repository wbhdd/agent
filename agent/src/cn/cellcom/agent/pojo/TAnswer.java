package cn.cellcom.agent.pojo;

/**
 * TAnswer entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TAnswer implements java.io.Serializable {

	// Fields

	private String id;
	private Integer no;
	private String questionId;
	private String answer;

	// Constructors

	/** default constructor */
	public TAnswer() {
	}

	/** full constructor */
	public TAnswer(String id, Integer no, String questionId, String answer) {
		this.id = id;
		this.no = no;
		this.questionId = questionId;
		this.answer = answer;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getNo() {
		return this.no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

}
