package cn.cellcom.agent.pojo;

/**
 * TGroupUser entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TGroupUser implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String groupId;
	private String user;
	private Integer switchPolicy;

	// Constructors

	/** default constructor */
	public TGroupUser() {
	}

	/** minimal constructor */
	public TGroupUser(String id, String org, String groupId, String user, Integer switchPolicy) {
		this.id = id;
		this.org = org;
		this.groupId = groupId;
		this.user = user;
		this.switchPolicy = switchPolicy;
	}

	/** full constructor */
	public TGroupUser(String id, String org, String pid, String groupId, String user, Integer switchPolicy) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.groupId = groupId;
		this.user = user;
		this.switchPolicy = switchPolicy;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getSwitchPolicy() {
		return this.switchPolicy;
	}

	public void setSwitchPolicy(Integer switchPolicy) {
		this.switchPolicy = switchPolicy;
	}

}
