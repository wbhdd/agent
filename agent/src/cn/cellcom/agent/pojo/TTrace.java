package cn.cellcom.agent.pojo;

import cn.cellcom.jar.mongo.IMongoPojo;

/**
 * TTrace entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TTrace implements java.io.Serializable, IMongoPojo {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String crm;
	private String title;
	private String url;
	private String logon;
	private Long createTime;
	private String requestAgent;
	private String httpSessionId;
	private String ip;

	// Constructors

	/** default constructor */
	public TTrace() {
	}

	/** minimal constructor */
	public TTrace(String id, String org, String pid, Long createTime, String httpSessionId) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.createTime = createTime;
		this.httpSessionId = httpSessionId;
	}

	/** full constructor */
	public TTrace(String id, String org, String pid, String crm, String title, String url, String logon, Long createTime, String requestAgent,
			String httpSessionId, String ip) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.crm = crm;
		this.title = title;
		this.url = url;
		this.logon = logon;
		this.createTime = createTime;
		this.requestAgent = requestAgent;
		this.httpSessionId = httpSessionId;
		this.ip = ip;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getCrm() {
		return this.crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLogon() {
		return this.logon;
	}

	public void setLogon(String logon) {
		this.logon = logon;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getRequestAgent() {
		return this.requestAgent;
	}

	public void setRequestAgent(String requestAgent) {
		this.requestAgent = requestAgent;
	}

	public String getHttpSessionId() {
		return this.httpSessionId;
	}

	public void setHttpSessionId(String httpSessionId) {
		this.httpSessionId = httpSessionId;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String get_IdName() {
		return "id";
	}

}