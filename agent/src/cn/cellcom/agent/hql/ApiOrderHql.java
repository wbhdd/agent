package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.entity.TSettingEntity;
import cn.cellcom.agent.online.client.ClientManager;
import cn.cellcom.agent.online.client.VisitorClient;
import cn.cellcom.jar.dao.IHql;

public class ApiOrderHql implements IHql {

	public String getHql(HttpServletRequest req, Object fm) {
		String cid = (String) req.getSession().getAttribute(AgentConstant.CRM_ID_IN_SESSION);
		VisitorClient visitorClient = (VisitorClient) ClientManager.getInstance().getClient(cid);
		String pid = req.getParameter("pid");
		StringBuffer sb = new StringBuffer();
		sb.append(req.getParameter("type")).append(",");
		sb.append(visitorClient.getCrm().getTid()).append(",");
		TSettingEntity setting = visitorClient.getSessionByPid(pid).getSetting();
		sb.append(setting.getAccessKey()).append(",");
		sb.append(setting.getCallUrl()).append(",");
		return sb.toString();
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		return new Object[] {null, null};
	}
}
