package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TTrace;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;

public class TTraceMql implements IHql {

	public String getHql(HttpServletRequest req, Object fm) {
		StringBuffer sb = new StringBuffer("from " + TTrace.class.getCanonicalName() + " where (pid = ?)");
		sb.append(" order by createTime desc");
		return sb.toString();
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();

		Object[] obj = new Object[] {};
		obj = AU.append(obj, user.getPid());

		return obj;
	}
}
