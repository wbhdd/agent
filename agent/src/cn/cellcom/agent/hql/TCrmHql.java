package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.jar.util.form.FormUtil;
import cn.cellcom.jar.util.form.FormUtil.SpecialField;
import cn.cellcom.jar.util.form.FormUtil.SpecialValue;

public class TCrmHql implements IHql {

	private Log log = LogFactory.getLog(this.getClass());

	public String getHql(HttpServletRequest req, Object fm) {
		FormUtil fu = new FormUtil();
		try {
			if ("select2".equals(req.getParameter("from"))) {
				LogonSession ls = new LogonSession(req);
				TUser user = (TUser) ls.getLogonObject();

				String term = CU.valueOf(req.getParameter("term"));
				StringBuffer sb = new StringBuffer("from TCrm where org='").append(user.getOrg()).append("' and (name like '%").append(term).append(
						"%' or phone like '%").append(term).append("%')");
				return sb.toString();
			}

			StringBuffer sb = new StringBuffer("from TCrm where 1=1 ");
			SpecialField sf = null;
			sf = fu.newSpecialField("name", "name", "like");
			sf.add("phone", "phone", "like");

			sb.append(fu.fieldToString(fu.getInput((ActionForm) fm, new String[] { "ids" }), null, null, sf));
			System.out.println(sb);
			return sb.append(" and org=? order by createTime desc").toString();
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		if ("select2".equals(req.getParameter("from"))) {
			return new Object[]{};
		}
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();
		FormUtil fu = new FormUtil();
		try {
			SpecialValue sv = null;
			sv = fu.newSpecialValue("name", "%", "%", null);
			sv.add("phone", "%", "%", null);

			Object[] obj = fu.valueToList(fu.getInput((ActionForm) fm, new String[] { "ids" }), sv).toArray();
			return AU.append(obj, new String[] { user.getOrg() });
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}
}
