package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;

import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TSessionForm;
import cn.cellcom.jar.util.form.FormUtil;
import cn.cellcom.jar.util.form.FormUtil.SpecialField;
import cn.cellcom.jar.util.form.FormUtil.SpecialValue;

public class TSessionHql implements IHql {

	private Log log = LogFactory.getLog(this.getClass());

	public String getHql(HttpServletRequest req, Object fm) {
		FormUtil fu = new FormUtil();

		LogonSession ls = new LogonSession(req);
		try {
			TUser user = (TUser) ls.getLogonObject();
			StringBuffer sb = new StringBuffer("from TSession as s, TCrm as c, TGroup g where s.crm=c.id and s.requestGroup=g.id ");
			sb.append(" and s.pid='").append(user.getPid()).append("'");
			
			SpecialField sf = fu.newSpecialField("c.phone", "c.phone", "like");
			sf = sf.add("s.endType", "s.endType", "=");
			sf = sf.add("s.step", "s.step", "=");

			sb.append(fu.fieldToString(fu.getInput((ActionForm) fm, new String[] { "ids" }), null, null, sf));
			return sb.append(" order by s.requestTime desc").toString();
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		LogonSession ls = new LogonSession(req);
		FormUtil fu = new FormUtil();
		TSessionForm form = (TSessionForm) fm;
		try {
			SpecialValue sv = null;
			sv = fu.newSpecialValue("c.phone", "%", "%", null);

			Object[] obj = new Object[] {};
			return AU.append(obj, fu.valueToList(fu.getInput((ActionForm) fm, new String[] { "ids" }), sv).toArray());
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}
}
