package cn.cellcom.jar.gen;

/**
 * 这个是协助生成EntryGen所需要的config.txt 采用gen2 web项目生成更为方便，这个类是初期使用
 * 
 * @author Administrator
 * 
 */
@Deprecated
public class ConfigGen {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ConfigTxtGen.prefix = "badanum";
		ConfigTxtGen.prefixshow = "黑名单";
		ConfigTxtGen.table = "BadANum";
		ConfigTxtGen.tableshow = "黑名单";
		ConfigTxtGen.pojo = "Badanum";
		ConfigTxtGen.list = "Badanum/list.xls";
		ConfigTxtGen.add = "Badanum/add.xls";
		ConfigTxtGen.gen("C:/hmd/config.txt");

		ConfigTxtGen.prefix = "badanum";
		ConfigTxtGen.prefixshow = "黑名单";
		ConfigTxtGen.table = "BadANumRecord";
		ConfigTxtGen.tableshow = "删除记录";
		ConfigTxtGen.pojo = "Badanumrecord";
		ConfigTxtGen.list = "Badanumrecord/list.xls";
		ConfigTxtGen.delete = "";
		ConfigTxtGen.gen("C:/hmd/config.txt");

		ConfigTxtGen.prefix = "whiteanum";
		ConfigTxtGen.prefixshow = "白名单";
		ConfigTxtGen.table = "WhiteGuojiANum";
		ConfigTxtGen.tableshow = "白名单";
		ConfigTxtGen.pojo = "Whiteguojianum";
		ConfigTxtGen.list = "Whiteguojianum/list.xls";
		ConfigTxtGen.add = "Whiteguojianum/add.xls";
		ConfigTxtGen.gen("C:/hmd/config.txt");

		ConfigTxtGen.prefix = "whiteanum";
		ConfigTxtGen.prefixshow = "白名单";
		ConfigTxtGen.table = "WhiteGuojiANumRecord";
		ConfigTxtGen.tableshow = "删除记录";
		ConfigTxtGen.pojo = "WhiteGuojiANumRecord";
		ConfigTxtGen.list = "WhiteGuojiANumRecord/list.xls";
		ConfigTxtGen.delete = "";
		ConfigTxtGen.gen("C:/hmd/config.txt");
	}

}
