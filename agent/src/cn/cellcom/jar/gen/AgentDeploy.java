package cn.cellcom.jar.gen;


import java.math.BigDecimal;
import java.util.List;

import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileProcessor;
import cn.cellcom.jar.file.ZipUtil;
import cn.cellcom.jar.file.IFileProcessor.MODE;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.MyException;

public class AgentDeploy {

	public static void main(String[] args) {
		IFileProcessor fip = new FileProcessor();
		try {
			String source = "C:/work/biz/agent";
			String inTomcat = "C:/work/software/apache-tomcat-6.0.36/webapps/agent";
			boolean existInTomcat = fip.pathExist(inTomcat, false);

			String inDesktop = "C:/Users/zhengyj/Desktop/agent";

			System.out.println("delete the desktop zipfile");
			fip.deleteFile(inDesktop + ".zip");
			String newVersionOfEb = null;
			if (existInTomcat) {
				System.out.println("handle agent...");
				newVersionOfEb = addVersion(inTomcat, source + "/src/version.sn");

				fip.copyP2P(inTomcat, inDesktop + "", MODE.COPY_DELETE_MODE);
				fip.copyF2F(inDesktop + "0/logon.html", inDesktop + "/logon.html", MODE.COPY_COVER_MODE);
				fip.copyF2F(inDesktop + "0/index.html", inDesktop + "/index.html", MODE.COPY_COVER_MODE);
				fip.copyF2F(inDesktop + "0/function.html", inDesktop + "/function.html", MODE.COPY_COVER_MODE);
				fip.copyF2F(inDesktop + "0/WEB-INF/classes/conf/common/db.properties", inDesktop + "/WEB-INF/classes/conf/common/db.properties",
						MODE.COPY_COVER_MODE);
				fip.copyF2F(inDesktop + "0/WEB-INF/classes/conf/common/db2.properties", inDesktop + "/WEB-INF/classes/conf/common/db2.properties",
						MODE.COPY_COVER_MODE);
				fip.copyF2F(inDesktop + "0/WEB-INF/classes/conf/common/env.properties", inDesktop + "/WEB-INF/classes/conf/common/env.properties",
						MODE.COPY_COVER_MODE);
			}
			ZipUtil zu = new ZipUtil();
			if (existInTomcat) {
				System.out.println("zip ebookig");
				zu.zip(inDesktop + "", inDesktop + newVersionOfEb + ".zip");
			}
			System.out.println("delete workspace file...");
			fip.deletePath("C:/work/software/apache-tomcat-6.0.36/webapps/ebooking");

			System.out.println("delete the desktop file");
			fip.deletePath(inDesktop);

			System.out.println("finished");
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String addVersion(String path, String file2) throws MyException {

		String versionfile = path + "/WEB-INF/classes/version.sn";
		IFileProcessor ifp = new FileProcessor();
		List<String> list = ifp.readList(versionfile);
		String v = list.get(0);
		BigDecimal v2 = CU.tob(v);
		v2 = v2.add(new BigDecimal(0.1));
		int l = String.valueOf(v2).indexOf(".") + 2;
		ifp.write(versionfile, String.valueOf(v2).substring(0, l), MODE.WRITE_CREATE_MODE);
		ifp.write(file2, String.valueOf(v2).substring(0, l), MODE.WRITE_CREATE_MODE);
		return String.valueOf(v2).substring(0, l);
	}
}
