<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>修改客户</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>	
	<script type="text/javascript" src="../js/TCrm.js"></script>
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">修改客户</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="table">
	    <div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">输入信息</div>
	    	<div id="tabtop-r1"></div>
      </div>
	</div>
	<div id="main-tab">
		<html:form action="/crm/TCrmDpAct?method=doModify" styleId="TCrmForm" >
			<html:hidden property='id' name='result' />
			<table width="911" align="center" class="table-slyle-hs" >
			<tr>
				<td>ID：</td>
				<td>${result.id}</td>
				</tr>
				<tr>
				<td>姓名：</td>
				<td><html:text property='name' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>电话：</td>
				<td><html:text property='phone' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>邮箱：</td>
				<td><html:text property='email' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>QQ：</td>
				<td><html:text property='qq' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>等级：</td>
				<td><myjsp:select name='level' added='class="s-select"' id='level' listname='levellist' value='${result.level}' readonly=' ' showonly=''/></td>
				</tr>
				<tr>
				<td>状态：</td>
				<td><myjsp:select name='status' added='class="s-select"' id='status' listname='statuslist' value='${result.status}' readonly=' ' showonly=''/></td>
				</tr>
				<tr>
				<td>性别：</td>
				<td><myjsp:select name='sex' added='class="s-select"' id='sex' listname='sexlist' value='${result.sex}' readonly=' ' showonly=''/></td>
				</tr>
				<tr>
				<td>企业：</td>
				<td><html:text property='company' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>省份：</td>
				<td><html:text property='province' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>城市：</td>
				<td><html:text property='city' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>备注：</td>
				<td><html:textarea property='memo' name='result' readonly='' styleId='memo' onkeyup="areaTextMaxLength('memo', 'memoDiv', 50)" rows='3' cols='50'/><div id='memoDiv'/></td>
				</tr>
				<tr>
				<td>CID：</td>
				<td><html:text property='tid' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>类型：</td>
				<td><myjsp:select name='type' added='class="s-select"' id='type' listname='typelist' value='${result.type}' readonly=' ' showonly=''/></td>
				</tr>
				<tr>
					<td align="right"><input name="submit" type="submit" class="search-2" value="修改"/></td>
					<td><input name="fanhui" type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>					 
				</tr>				
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
