<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>留言管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src="../plugins/layer/layer.js"></script>
	<script type="text/javascript" src="../js/online/all-tip.js"></script>

	<script>
		function showLogs(cid) {
			layer.open({
				type : 2,
				closeBtn : 1,
				title : "聊天历史",
				content : "../online/agent.jsp?onlyShow=true&cid=" + cid,
				move : false,
				maxmin : true,
				area : [ '80%', '80%' ],
				shadeClose : true,
				cancel: function(){ 
				    //右上角关闭回调
				    layer.closeAll();
				}
			});
		}
	</script>
</head>

<body>
	<html:form action="/biz/TLwDpAct?method=list" styleId="TLwForm">
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">留言管理</div>
				<div id="right"></div>
			</div>
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					<li>姓名：<html:text property='name' styleClass='s-input-2' /></li>
					<li>联系电话：<html:text property='phone' styleClass='s-input-2' /></li>
					<li>Email：<html:text property='email' styleClass='s-input-2' /></li>
					<li>留言：<html:text property='memo' styleClass='s-input-2' /></li>
				</div>
				<div id="info-4"> 
					<li>状态：<myjsp:select name='status' width='120' id='status' listname='statuslist' first='----' value='${TLwForm.status}' readonly='' showonly=''/></li>
					<li style="width:80px"><html:submit styleClass="search-2" value="查询" /></li>	
					<li style="width:80px"><input class='search-2' type='button' value='导出数据' onClick='return exportList("../biz/TLwDpAct.do?method=doExport")'></li>			
				</div>          
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li></li>
					<li></li>
					<li></li>					
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>渠道</td>
						<td>姓名</td>
						<td>联系电话</td>
						<td>Email</td>
						<td>状态</td>
						<td>QQ</td>
						<td>留言</td>
						<td>留言时间</td>
						<td>操作</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>${aRecord.channel}</td>
							<td class='zw-txt'>${aRecord.name}</td>
							<td class='zw-txt'>${aRecord.phone}</td>
							<td class='zw-txt'>${aRecord.email}</td>
							<td class='zw-txt'>
								<myjsp:select name='status' width='0' id='status' listname='statuslist' first='----' value='${aRecord.status}' readonly='' showonly='true'/>
								<div style="display: none" id="handleResultDiv">${aRecord.handleResult}</div>
								<img id="handleResult" src="../themes/online/how.png" onMouseOver="showDefinedTip('handleResult', '处理意见：<br/>' + $('#handleResultDiv').html())"/>
							</td>
							<td class='zw-txt'>${aRecord.qq}</td>
							<td class='zw-txt'>${aRecord.memo}</td>
							<td class='zw-txt'><myjsp:date value='${aRecord.createTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'>
								<a href='../biz/TLwDpAct.do?method=modify&id=${aRecord.id}'>修改</a>
								<c:if test="${aRecord.status == 'N'}">
									<a href='javascript:window.top.frames["topFrame"].toService()'>进入坐席处理</a>
								</c:if><br/>
								<a href=''>聊天历史记录</a>
								<a href='javascript:showLogs("${aRecord.crm}")'>聊天内容</a>
							</td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
