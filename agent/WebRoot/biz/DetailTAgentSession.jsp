<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>坐席会话详细信息</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">坐席会话详细信息</div>
			<div id="right"></div>
		</div>
	</div>
	
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">详细信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	
	<div id="main-tab">	
		<html:form action="/biz/TAgentSessionDpAct?method=doModify">	
		<html:hidden property='id' value='result[0].id' />
	
		<table width="911" align="center" class="table-slyle-hs" >	
			<tr>
				<td width="15%">渠道：</td>
				<td width="35%">${result[0].channel}</td>
				<td width="15%">请求组：</td>
				<td width="35%">${result[1].name}</td>
			</tr>	
			<tr>
				<td colspan="4"></td>
			</tr>
			<tr>
				<td>访客：</td>
				<td>${result[3].name}</td>
				<td>访客等级：</td>
				<td>${result[3].level}</td>
			</tr>
			<tr>
				<td>访客类型：</td>
				<td><myjsp:select name='type' width='120' id='type' listname='typelist' first='----' value='${result[3].type}' readonly='' showonly='true'/></td>
				<td>访客电话：</td>
				<td>${result[3].phone}</td>
			</tr>
			<tr>
				<td colspan="4"></td>
			</tr>
			<tr>
				<td>会话ID：</td>
				<td>${result[2].id}</td>
				<td>结束方式：</td>
				<td><myjsp:select name='endType' width='100' id='endType' listname='endTypelist' first='----' value='${result[2].endType}' readonly='' showonly='true'/></td>
			</tr>
			<tr>
				<td>满意度：</td>
				<td>${result[2].satisfyScore}</td>
				<td>满意度评论：</td>
				<td>${result[2].satisfyText}</td>
			</tr>
			<tr>
				<td colspan="4"></td>
			</tr>			
			<tr>
				<td>坐席：</td>
				<td>${result[0].agent}</td>
				<td>坐席昵称：</td>
				<td>${result[0].agentNickname}</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>接入时间：</td>
				<td><myjsp:date value='${result[0].joinTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				<td>接入方式：</td>
				<td><myjsp:select name='joinType' width='0' id='joinType' listname='joinTypelist' first='' value='${result[0].joinType}' readonly='' showonly='true'/></td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>结束时间：</td>
				<td><myjsp:date value='${result[0].endTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				<td>结束方式：</td>
				<td><myjsp:select name='endType' width='0' id='endType' listname='endTypelist' first='' value='${result[0].endType}' readonly='' showonly='true'/></td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>首次响应：</td>
				<td><myjsp:date value='${result[0].firstResponseTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				<td>消息数：</td>
				<td>${result[0].messageCount}</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="button" class="search-2" value="返回" onclick="history.back()" /></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</html:form>
	</div>
</div>
</body>
</html>
