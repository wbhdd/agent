<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>新增按钮管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/TButton.js"></script>	
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">新增按钮管理</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">输入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/self/TButtonDpAct?method=doAdd" styleId="TButtonForm" enctype="multipart/form-data">
			<p align="center">${result}</p>		
			<table width="911" align="center" class="table-slyle-hs" >		
<tr>
<td>图片：</td>
<td><html:file property='imgFile' styleClass='s-input' maxlength='40'/>(45*45)</td>
</tr>
<tr>
<td>链接：</td>
<td><html:text property='href' styleClass='s-input' maxlength='40'/></td>
</tr>
<tr>
<td>按钮名：</td>
<td><html:text property='name' styleClass='s-input' maxlength='40'/></td>
</tr>
<tr>
<td>按钮提示：</td>
<td><html:text property='title' styleClass='s-input' maxlength='40'/></td>
</tr>
<tr>
<td>内部备注：</td>
<td><html:textarea property='memo' styleId='memo' onkeyup="areaTextMaxLength('memo', 'memoDiv', 100)" rows='3' cols='50'/><div id='memoDiv'/></td>
</tr>

				<tr>
				<td align="right"><input type="submit" class="search-2" value="新增"/></td>
				<td><input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>
				</tr>
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
