<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage=""%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>新增自助聊天</title>
		<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />

		<!--请在下面增加js-->
		<script type="text/javascript" src="../js/mycommon.js"></script>
		<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src='../plugins/tinymce/tinymce.min.js'> </script>
		<script type="text/javascript" src='../plugins/tinymce/jquery.tinymce.min.js'> </script>
		<script type="text/javascript" src='../plugins/tinymce/tinymce-extend.js'> </script>
		<script type="text/javascript" src="../js/jquery.validate.js"></script>
		<script type="text/javascript" src="../js/jquery.mine.js"></script>
		<script type="text/javascript" src="../js/TQuestion.js"></script>
		<script>
			loadTinymce("answer");
		</script>
	</head>

	<body>
		<div id="main">
			<div id="tab-top">
				<div id="ptk1">
					<div id="lift"></div>
					<div id="pt">
						新增自助聊天
					</div>
					<div id="right"></div>
				</div>
			</div>
			<div id="table">
				<div id="ptk">
					<div id="tabtop-l"></div>
					<div id="tabtop-z">
						输入信息
					</div>
					<div id="tabtop-r1"></div>
				</div>
			</div>
			<div id="main-tab">
				<html:form action="/self/TQuestionDpAct?method=doAdd" styleId="TQuestionForm">
					<html:hidden property='type' value='${TQuestionForm.type}' />
					<html:hidden property='level' styleId="${TQuestionForm.level}" />
					<html:hidden property="subject" styleId="${TQuestionForm.subject}" />

					<p align="center">
						${result}
					</p>
					<table width="911" align="center" class="table-slyle-hs">
						<tr>
							<td>
								问题：
							</td>
							<td>
								<html:text property='question' styleId='question' style="width:400px;height:20px" maxlength="30"/>* 30个字符
							</td>
						</tr>
						<tr>
							<td>
								答案：
							</td>
							<td>
								<html:textarea property='answer' styleId='answer' rows='4' cols='70' />
								请控制字数,不要超过1000字符
							</td>
						</tr>
						<c:if test="${TQuestionForm.type == 'F'}">
							<tr>
								<td>
									提问次数：
								</td>
								<td>
									<html:text property='times' readonly='' style="width:40px;height:20px"/>*
							</tr>
						</c:if>
						<tr>
							<td align="right">
								<input type="submit" class="search-2" value="新增" />
							</td>
							<td>
								<input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'" />
							</td>
						</tr>
					</table>
				</html:form>
			</div>

		</div>
	</body>
</html>
