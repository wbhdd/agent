<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>自助聊天管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script>
		function sendThis(faqId) {
			parent.doSend($("#" + faqId).html());
		}
		function useThis(faqId) {
			parent.keditor.html(parent.keditor.html() + $("#" + faqId).html());
		}

		function searchFAQ(question) {
			$("#type").val("");//搜索所有
			$("#question").val(question);
			$("#TQuestionForm").submit();
		}
	</script>
</head>

<body>
	<html:form action="/self/TQuestionDpAct?method=list2" styleId="TQuestionForm">
			<div id="main-tab" style="left:0px">
				<div id="info-4">
					<li><myjsp:select name='type' width='48' height='20' id='type' listname='typelist' value='${TQuestionForm.type}' first='所有' readonly='' showonly='' added='onchange="TQuestionForm.submit();"'/></li>
					<li>问：<html:text property='question' style='width:80px' styleId="question"/></li>
					<li>答：<html:text property='answer' style='width:80px' /></li>
					<li style="width:80px"><html:submit styleClass="search" value="查询" /></li>	
					<li style="width:80px"></li>			
				</div>          
			</div>
			<div id="main-tablist" style="left:0px">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>

					<c:forEach var="aRecord" items="${result}">
						<tr>
							<td colspan="2">
								<c:if test="${TQuestionForm.type == 'F'}">
									[${aRecord.times}]
								</c:if>
								${aRecord.question}?
							</td>
						</tr>
						<tr class='out' style="text-align: left">
							<td class='zw-txt' width="90%" id='faq_${aRecord.id}'>${aRecord.answer}</td>
							<td>
								<a href='javascript:useThis("faq_${aRecord.id}")'>编辑</a><br/>
								<a href='javascript:sendThis("faq_${aRecord.id}");'>发送</a><br/>
							</td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
	</html:form>
</body>
</html>
