<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>修改按钮管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src='../plugins/jquery.form.js'> </script>	
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>	
	<script type="text/javascript" src="../js/TButton.js"></script>
  	<script type="text/javascript" src="../js/my.upload.js"></script>
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
   	<script type="text/javascript" src="../js/online/constant.js"></script>
	<script>
		var oldImg = '${result.img}';
		function uploadImg(token) {
			$("#changeResult").html("");
			var params = {
				token : token,
				uuid : "uuid-" + (new Date().getTime()),
				bizType : "SETTING",
				multType : "IMG",
				uploadUrl : UPLOAD_URL
			};
			MyUpload.uploadFile(params, $("#TButtonForm"), function(resp) {
				if(resp.code == 0) {//上传成功则处理存储，并展示最新的图片和删除旧的图片
					commit('../self/TButtonDpAct.do?method=doModify&ajax=true', {id:'${result.id}', 'img':resp.newName}, function(resp2){
						if(resp2.code == 0) {
							$("#orgImg").attr("src", "../file/load.do?method=get&bizType=SETTING&fileName=" + resp.newName);
							commit('../file/load.do?method=doDelete&bizType=SETTING&fileName=' + oldImg);
							oldImg = resp.newName;
							$("#changeResult").html("修改生效");
						} else {
							alert("上传失败");
						}
					});
				} else {
					alert("上传失败：" + resp.msg);
				}
				$("#file").val("");
			});
		}
	</script>	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">修改按钮管理</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="table">
	    <div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">输入信息</div>
	    	<div id="tabtop-r1"></div>
      </div>
	</div>
	<div id="main-tab">
		<html:form action="/self/TButtonDpAct?method=doModify" styleId="TButtonForm" >
			<html:hidden property='id' name='result' />
			<table width="911" align="center" class="table-slyle-hs" >
				<tr>
				<td>图片：</td>
					<td>
						<img src="../file/load.do?method=get&bizType=SETTING&fileName=${result.img}" id="orgImg" width="45px" height="45px"/>
            <input id="file" name="file" type="file" value="" onchange="uploadImg('${token}')" style="width:67px">(选择头像更换)    
            <br/><span id="changeResult"><span/>   
					</td>
				</tr>				
				<tr>
				<td>序号：</td>
				<td><html:text property='seq' name='result' readonly='' styleClass='s-input'/>[提示：序号越小排序越前]</td>
				</tr>
				<tr>
				<td>链接：</td>
				<td><html:text property='href' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>按钮名：</td>
				<td><html:text property='name' name='result' readonly='' styleClass='s-input'/></td>
				</tr>
				<tr>
				<td>按钮提示：</td>
				<td><html:textarea property='title' name='result' readonly='' styleId='title' onkeyup="areaTextMaxLength('title', 'titleDiv', 40)" rows='3' cols='50'/><div id='titleDiv'/></td>
				</tr>
				<tr>
				<td>内部备注：</td>
				<td><html:textarea property='memo' name='result' readonly='' styleId='memo' onkeyup="areaTextMaxLength('memo', 'memoDiv', 100)" rows='3' cols='50'/><div id='memoDiv'/></td>
				</tr>
				<tr>
					<td align="right"><input name="submit" type="submit" class="search-2" value="修改"/></td>
					<td><input name="fanhui" type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>					 
				</tr>				
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
