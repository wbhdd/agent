<%@ page contentType="text/html; charset=GBK" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
	<title>DDD管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script language="javascript" type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script language="javascript" type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/ddDD/TTagDpAct?method=list" styleId="TTagForm">
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">DDD管理</div>
				<div id="right"></div>
			</div>
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					
					<li style="width:80px"></li>	
					<li style="width:80px"></li>			
				</div>          
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li></li>
					<li><input class='' type='button' value='删除选中' onClick='return deleteSelected("../ddDD/TTagDpAct.do?method=doDelete")'></li>
					<li></li>					
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
<td>选择</td>
<td>id</td>
<td>企业</td>
<td>组号</td>
<td>tagTxt</td>
<td>fatherId</td>

					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
<td class='zw-txt'><html:checkbox property='ids' value='${aRecord.id}' /></td>
<td class='zw-txt'>${aRecord.id}</td>
<td class='zw-txt'>${aRecord.org}</td>
<td class='zw-txt'>${aRecord.no}</td>
<td class='zw-txt'>${aRecord.tagTxt}</td>
<td class='zw-txt'>${aRecord.fatherId}</td>

						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
