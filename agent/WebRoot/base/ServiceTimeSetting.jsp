<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>服务时间</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">服务时间</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>配置模式：</td>
							<td class='zw-txt' align="left">
								<myjsp:select name='serviceSettingMode' added='class="s-select"' id='serviceSettingMode' listname='modelist' value='${result.serviceSettingMode}' readonly='' showonly='' />						
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>工作日期：</td>
							<td class='zw-txt' align="left">
								<myjsp:scheckbox name='serviceDay' id='serviceDay1' value='${result.serviceDay}' rvalue='1'/>周一
								<myjsp:scheckbox name='serviceDay' id='serviceDay2' value='${result.serviceDay}' rvalue='2'/>周二			
								<myjsp:scheckbox name='serviceDay' id='serviceDay3' value='${result.serviceDay}' rvalue='3'/>周三
								<myjsp:scheckbox name='serviceDay' id='serviceDay4' value='${result.serviceDay}' rvalue='4'/>周四
								<myjsp:scheckbox name='serviceDay' id='serviceDay5' value='${result.serviceDay}' rvalue='5'/>周五
								<myjsp:scheckbox name='serviceDay' id='serviceDay6' value='${result.serviceDay}' rvalue='6'/>周六
								<myjsp:scheckbox name='serviceDay' id='serviceDay7' value='${result.serviceDay}' rvalue='7'/>周日
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>工作时间：</td>
							<td class='zw-txt' align="left">
								上午：<myjsp:select name='A1' width='40' id='A1' listname='hourList23' value='${A1}' readonly='' showonly='' />:<myjsp:select name='A2' width='40' id='A2' listname='minutList59' value='${A2}' readonly='' showonly='' />
									--<myjsp:select name='A3' width='40' id='A3' listname='hourList23' value='${A3}' readonly='' showonly='' />:<myjsp:select name='A4' width='40' id='A4' listname='minutList59' value='${A4}' readonly='' showonly='' />
								下午：
								<myjsp:select name='B1' width='40' id='B1' listname='hourList23' value='${B1}' readonly='' showonly='' />:<myjsp:select name='B2' width='40' id='B2' listname='minutList59' value='${B2}' readonly='' showonly='' />
									--<myjsp:select name='B3' width='40' id='B3' listname='hourList23' value='${B3}' readonly='' showonly='' />:<myjsp:select name='B4' width='40' id='B4' listname='minutList59' value='${B4}' readonly='' showonly='' />
							</td>
						</tr>		
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>下班拒接会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:scheckbox name='offdutyReject' id='offdutyReject' value='${result.offdutyReject}' />
								开启之后，下班时间则直接拒绝访客接入
							</td>
						</tr>		
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>拒绝会话提示语：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='offdutyRejectTip' id='offdutyRejectTip' value='${result.offdutyRejectTip}' add="style='width:400px'" line='3'/>		
							</td>
						</tr>
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>下班接入会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='offdutyReceiptTip' id='offdutyReceiptTip' value='${result.offdutyReceiptTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>	
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
