<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>扩展配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
  <script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	
	<%
		String host = request.getLocalAddr();
		int port = request.getServerPort();
		String path = request.getContextPath();
	%>	
</head>

<body onload="show()">
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">访客端扩展</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>						
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>嵌入客户端：</td>
							<td class='zw-txt' align='left'>								
								请在您的网页底部嵌入以下代码，【请注意正确填写您的<a href="../base/TChannelDpAct.do?method=list">渠道号channel参数</a>】<br/>
								<p/>&lt;script type="text/javascript" src="http://<%=host%>:<%=port%><%=path%>/js/online/visitor-access.js?pid=${attribute.pid}&channel=10001"&gt;&lt;/script&gt;
								<p/>&lt;script type="text/javascript" src="http://<%=host%>:<%=port%><%=path%>/plugins/layer/layer.js"&gt;&lt;/script&gt;
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>访客实名对接：</td>
							<td class='zw-txt' align='left'>
								如果您期望进入的访客具备身份识别<br/>
								1、请在您的页面提供以下方法给客服系统调用：<br/>
								function jianyueInitData() {<br/>
									var json = {<br/>
										tid : "您的用户ID",<br/>
										timestamp : "时间戳",<br/>
										ttoken : "您的token"//用于给客服系统识别本次请求的有效性，MD5(tid + timestamp + accesskey)，accesskey请参考<a href="../base/TSettingDpAct.do?method=list&tag=EXTEND&level=1">“扩展基础”</a><br/>
									};<br/>
									return json;<br/>
								}<br/>
								2、请到<a href="../base/TSettingDpAct.do?method=list&tag=EXTEND&level=1">“扩展基础”</a>中填写您的回调地址【实现获取CRM】，供客服系统获取CRM的精准信息<br/>
							</td>
						</tr>
				</table>
				
				<div align="center"> 
					<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
