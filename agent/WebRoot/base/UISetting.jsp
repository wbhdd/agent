<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>接入配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src='../plugins/jquery.form.js'> </script>	
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
   	<script type="text/javascript" src="../js/my.upload.js"></script>
   	<script type="text/javascript" src="../js/online/constant.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script>
		var oldHeader = '${result.orgHeader}';
		function uploadHeader(token) {
			var params = {
				token : token,
				uuid : "uuid-" + (new Date().getTime()),
				bizType : "SETTING",
				multType : "IMG",
				uploadUrl : UPLOAD_URL
			};
			MyUpload.uploadFile(params, $("#CommonForm"), function(resp) {
				if(resp.code == 0) {//上传成功则处理
					commit('../base/TSettingDpAct.do?method=doModifyOrgSetting', {k:'orgHeader', 'orgHeader':resp.newName, 'level' : $('#level').val(), 'subject' : $('#subject').val()}, function(resp2){
						if(resp2.code == 0) {
							$("#orgHeader").attr("src", "../file/load.do?method=get&bizType=SETTING&fileName=" + resp.newName);
							commit('../file/load.do?method=doDelete&bizType=SETTING&fileName=' + oldHeader);
							oldHeader = resp.newName;
						} else {
							alert("上传失败");
						}
					});
				} else {
					alert("上传失败：" + resp.msg);
				}
				$("#file").val("");
			});
		}
	</script>

</head>

<body>
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm" enctype="multipart/form-data">
		<html:hidden property="tag" styleId="tag"/>
		<html:hidden property="level" styleId="level"/>
		<html:hidden property="subject" styleId="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">UI配置</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>窗口主题颜色：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='windowBackgroundColor' id='windowBackgroundColor' value='${result.windowBackgroundColor}' add="class='s-input-2'" line='1'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>窗口打开模式：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='windowOpenType' id='windowOpenType' listname='windowOpenTypeList' value='${result.windowOpenType}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>窗口显示模式：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='windowShowType' id='windowShowType' listname='windowShowTypeList' value='${result.windowShowType}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>窗口宽度*高度：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='windowWidth' id='windowWidth' value='${result.windowWidth}' add="style='width:30px;height:20px'" line='1'/>*<myjsp:sinput name='windowHeight' id='windowHeight' value='${result.windowHeight}' add="style='width:30px;height:20px'" line='1'/>
								【左右栏目显示】最优宽高：880*600，【单聊天窗栏目】最优宽高：400*480
							</td>
						</tr>						
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>按钮抬头（如：快捷访问）：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='buttonTag' id='buttonTag' value='${result.buttonTag}' add="class='s-input-2'" line='1'/><a href="../self/TButtonDpAct.do?method=list&level=1" target="mainFrame">按钮列表</a>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>菜单页抬头（如：常见问题）：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='firstMenuTag' id='firstMenuTag' value='${result.firstMenuTag}' add="class='s-input-2'" line='1'/><a href="../self/TQuestionDpAct.do?method=list&level=1&type=F" target="mainFrame">列表内容</a>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>底部签名：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='supportHolder' id='supportHolder' value='${result.supportHolder}' add="class='s-input-2'" line='1'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>底部签名的链接：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='supportUrl' id='supportUrl' value='${result.supportUrl}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>企业头像：</td>
							<td class='zw-txt' align='left'>
								<img src="../file/load.do?method=get&bizType=SETTING&fileName=${result.orgHeader}" id="orgHeader" width="50px" height="50px"/>
								<input id="file" name="file" type="file" value="" onchange="uploadHeader('${token}')" style="width:67px">(选择头像更换)                    
							</td>
						</tr>				
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>指引按钮文字（如：在线客服）：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='guideButtonText' id='guideButtonText' value='${result.guideButtonText}' add="class='s-input-2'" line='1'/>
							</td>
						</tr>
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
