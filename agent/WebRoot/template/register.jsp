<%@ page language="java" contentType="text/html;charset=gbk"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>用户注册</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<link rel="stylesheet" type="text/css" href="themes/skyblue/index3.css" />
		
		<script type="text/javascript" src="plugins/jquery-x.x.js"></script>	
		<script type="text/javascript" src="js/jquery.validate.js"></script>					
		<script type="text/javascript" src="js/jquery.mine.js"></script>							
		<script type="text/javascript" src="js/register.js"></script>	
		<script>
			function isExist(username) {
				var data0 = "";
				$.ajax({
					type: "POST",
					url: "Logon/LogonDpAct.do?method=isExist&username=" + username,
					success: function(data){							
						alert(data);
					}, 
					error:function(data){
			
					}
				});
			}			
		</script>								
	</head>

	<body style="background-color:#e1efff" onLoad="javascript:LogonForm.username.focus();">
		<html:form action="/Logon/LogonDpAct?method=register" styleId="LogonForm">
		<div id="maindiv">
		  <div id="indexbg">
		   <div id="bg-z"></div>
			<div id="bg-main">				
				<div id="biaodan" style="width:400px">
				    <div>用户名　：<input name="username" type="text" class="textfie" id="username" value="" maxlength="30" style="width:170px"/><span style="cursor:pointer" onClick="isExist(LogonForm.username.value)">检测</span>*</div>				
					#{registerSelfInfoRows}																						
					<div>验证码　：<input name="validate" type="text" class="textfie1" id="validate" maxlength="4" />*
					<img id="validateNumberPic" border=0 src="Logon/LogonDpAct.do?method=getValidateNumber&${validateNumberOfSystem}"><span class="style0" onClick="validateNumberPic.src=validateNumberPic.src+'&'" style="cursor:pointer">刷新</span>
					</div>					
			  </div>
				<div id="biaodan" align="center">
				<input type="submit" value="注&nbsp;册"/>
				<input type="button" onClick="history.back()" value="返&nbsp;回"/>			
   			  </div>
			</div>
			<div id="bg-y"></div>
		  </div>		  
		</div>
		<input type="image" src="themes/images/images3/h-ny2.gif" style="height:0px;"/>
		</html:form>
	</body>
</html>

