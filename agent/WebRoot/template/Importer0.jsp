<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>中国电信118166多方通</title>

<link href="../css/main.css" rel="stylesheet" type="text/css" />
<!--请在下面增加js-->
<script type="text/javascript" src="../js/mycommon.js"></script>
<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jquery.mine.js"></script>
<script type="text/javascript" src="../js/#{filename}.js"></script>	
</head>

<body>
<html:form action="/#{prefix}/#{filename}DpAct?method=doImporter" method="post" enctype="multipart/form-data" styleId="#{pojo}Form">
	<div id="main-kj">
		<div id="dft-td">
			<h2 style="margin-top: 15px"><img src="../images/group.gif" width="206" height="22" /></h2>
			<div id="groupkj" style="margin-bottom: 10px;" >    
				<div id="pt-one">导入#{tableshow}：&nbsp;</div>				
				<div id="line-02"></div>
				<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0" class="tab-sy">
					<tr>
					  <td>模板：</td>
					  <td><a href="/#{prefix}/#{filename}DpAct.do?method=tempalte">导入必须按照模板格式输入，点击下载模板</a></td>
				  </tr>
					<tr>
						<td width="30%">选择：</td>
						<td>
						#{importerrows}						
						</td>						
					</tr>				
					<tr>
						<td width="30%">选择文件：</td>
						<td><input name="importerfile" type="file" style="width:400px"/></td>						
					</tr>
				</table>	
				<li style="width:300px">&nbsp;</li>
				<li><input type="button" value="返 回" onclick="history.back()" class="ny-2" tyle="cursor:pointer"/></li>
				<li style="width:20px">&nbsp;</li>
				<li><input type="submit" value="导 入" class="ny-2" tyle="cursor:pointer"/></li>				
			</div><!--end <div id="groupkj>-->
        </div>
		<!--end <div id="dft-td">--->
	</div>
</html:form>	
</body>
</html>
