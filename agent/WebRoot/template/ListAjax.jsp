<%@ page contentType="text/html; charset=GBK" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
	<title>#{tableshow}管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	<link rel="alternate stylesheet" href="../themes/orangeyellow/orangeyellowMain.css" type="text/css" title="styles2" />    
	<link rel="alternate stylesheet" href="../themes/darkblue/darkblueMain.css" type="text/css" title="styles3" />    
	<link rel="alternate stylesheet" href="../themes/grayblue/grayblueMain.css" type="text/css" title="styles4" />    
	<link rel="alternate stylesheet" href="../themes/earchyellow/earchyellowMain.css" type="text/css" title="styles5" />  
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
<script>
function doGet() {
	var data0 = "";
#{getdatavalue}
	$.ajax({
		type: "POST",
		data: data0,
		dataType: 'xml',
		url: "../#{prefix}/#{filename}DpAct.do?method=list",
		success: function(data){							
			//删除表格中的有效数据
			for(var v=showData.rows.length-1;v>=2;v--) {
				showData.deleteRow(v);
			}				
			$(data).find("#{pojo}").each(function(){         //找到根节点
				var row = showData.insertRow(showData.rows.length);
				row.id="minpt-tab";
#{setpojoproperties}
		    });
		}, 
		error:function(data){

		}
	});
}

//可以开放该方法做自动执行
//setInterval("doGet()", 10000);
</script>
</head>

<body>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">#{tableshow}管理</div>
				<div id="right"></div>
			</div>
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					#{searchrows}
					<li style="width:80px"><input type="button" styleClass="search" value="查询" onclick="doGet()"/></li>			
				</div>          
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>			
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
#{listrowstitle}
					</tr>
				</table>
			</div>
		</div>
</body>
</html>
