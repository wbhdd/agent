<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage=""%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>坐席监控</title>
		<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
		<!--请在下面增加js-->
		<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
		<script type="text/javascript" src='../js/mycommon.jquery.js'> </script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src='../js/online/all-tip.js'> </script>
		<script type="text/javascript" src='../js/online/constant.js'> </script>

		<script>
			function showSessions(filterAgent) {
				layer.open({
					type : 2,
					closeBtn : 1,
					title : "坐席会话监控",
					content : "../monitor/session.jsp?filterAgent=" + filterAgent,
					move : false,
					maxmin : true,
					area : [ '80%', '80%' ],
					shadeClose : true,
					cancel: function(){ 
					    //右上角关闭回调
					    layer.closeAll();
					}
				});
			}
			
			
			var counting = 0;
			function readAgents(){
				commit('../monitor/MonitorDpAct.do?method=agents', {}, function(resp){
					if(resp) {
						for(var i = 0; i< resp.length ;i++) {
							var one = resp[i];
							var td = "";
							td = td + "		<td>" + one.agent.no + "</td>";
							td = td + "		<td>" + one.agent.nickName + "</td>";
							td = td + "		<td>" + one.loginTime + "/" + one.loginDuration + "</td>";
							td = td + "		<td>" + AGENT_TEXT[one.config.status] + "</td>";
							td = td + "		<td>" + one.talking; +  "/" + one.config.maxChat + "</td>";
							td = td + "		<td>" + one.sessionsTime + "</td>";
							td = td + "		<td>" + one.sessionMaxTime + "</td>";
							var m = one.talking == 0 ? 0 : one.sessionsTime / one.talking;
							td = td + "		<td>" + m + "</td>";
								
							var agent = $("#tr_" + one.agent.username);
							if(agent.length > 0) {//已经存在则更新
								agent.html(td);
							} else {//不存在则增加tr
								//var tr = "<tr class='out' onMouseOver='this.className=\"over\"' onMouseOut='this.className=\"out\"' id='tr_" + one.agent.username + "'>";
								
								var onMouseOver = 'this.className="over"';
								var onMouseOut = 'this.className="out"';
								var onClick = 'javascript:showSessions("' + one.agent.username + '")';
								var tr = "<tr class='out' onMouseOver='" + onMouseOver + "' onMouseOut='" + onMouseOut + "' id='tr_" + one.agent.username + "' onClick='" + onClick + "'>";
								tr = tr + td;
								tr = tr + "	</tr>";
								$("#agents-table").append(tr);
							}
						}
						$("tr").each(function(){
						  if(this.id.indexOf("tr_") >= 0) {
						  	var exist = false;
						  	for(var i = 0; i< resp.length ;i++) {
								var username = "tr_" + resp[i].agent.username;
								if(username == this.id) {
									exist = true;
								}
							}
							if(exist == false) {//如果已经不存在了，就删除
								$(this).remove();
							}
						  }
						});
					}
					counting++;
					$("#counting").html(counting);
					setTimeout(readAgents, 5000);			
				});
			}
			
			readAgents();
		</script>
	</head>

	<body>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">
					坐席监控[<span id='counting'></span>]
				</div>
				<div id="right"></div>
			</div>

			<div id="table">
				<div id="ptk">
					<div id="tabtop-l">
					</div>
				</div>
			</div>

			<div id="fh-flie-2">
			</div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='agents-table'>
					<tr id='minpt-tab'>
						<td width="12%" class="zw-txt">
							坐席工号
						</td>
						<td width="12%" class="zw-txt">
							坐席昵称
						</td>
						<td width="12%" class="zw-txt">
							本次登陆时间[时长]
						</td>
						<td width="12%" class="zw-txt">
							状态
						</td>
						<td width="12%" class="zw-txt">
							当前接待数
						</td>
						<td width="12%" class="zw-txt">
							会话总时长
						</td>
						<td width="12%" class="zw-txt">
							会话最大时长
						</td>
						<td width="12%" class="zw-txt">
							会话平均时长
						</td>
					</tr>
				</table>

				<div id="info-pz">
					${link}
				</div>
			</div>
		</div>
	</body>
</html>
