function onsubmitCheck(){
	if(checkNull('newName','名称','newDesc','描述','newIden','标识')==false){
	    return false;
	}
    makeSubmitField(getNewArrayFromTree('root'), "forSubmitNewNodes");
	if(confirm("确定要提交吗?")){
		return true;
	}
	return false;
}

function addTreeNode(){
	var d = new Date();
	var newName = $$('newName');
	var newDesc = $$('newDesc');
	var newIden = $$('newIden');
	
	if(checkNull('newName','名称','newDesc','描述','newIden','标识')==false){
	    return ;
	}
	
	if(/^[a-z|A-Z|0-9]+$/.test(newIden.value)==false){
		alert('请输入英文字母格式');
		newIden.focus();
	    return;
	}
	
	var fatherItemId = tree2.getSelectedItemId();
	if(fatherItemId.match(/root/)) {
		if(fatherItemId != "root") {
			alert("要在组下面增加节点，请先提交组的操作然后再继续");
			return;
		}
	} else {
		if(!fatherItemId.match(/O_/) && !fatherItemId.match(/G_/)){
			alert('只允许添加至三层结点');
			return ;
		}
		if(fatherItemId.match('N_[0-9]{1,6}-O_[0-9]{1,6}')){
			alert('只允许添加至三层结点.');
			return ;
		}
		if(fatherItemId.match('N_[0-9]{1,6}-N_[0-9]{1,6}-G_[0-9]{1,6}')){
			alert('只允许添加至三层结点.');
			return ;
		}
	}
	
	//如果当前结点有子结点，展开
	tree2.openItem(fatherItemId);
	var newItemId = 'N_'+d.getTime()%10000+"-"+fatherItemId;
	tree2.insertNewItem(fatherItemId,newItemId,newName.value,0,0,0,0,'');
	tree2.setUserData(newItemId,"desc",newDesc.value);
	tree2.setUserData(newItemId,"iden",newIden.value);
	
	tree2.setItemColor(newItemId,'#ff0000','#ff0000');
	//设置隐藏
}

function getNewArrayFromTree(treeRootId){
	//先确定要动态增加的权限
    var allItemIds = tree2.getAllSubItems(treeRootId);
    var arr = allItemIds.split(",");
	
	var newArr = new Array();
	var k = 0;
	for(var i=0; i<arr.length; i++) {
		if(arr[i].indexOf("N_")==0 || arr[i].match("N_[0-9]{6}-N_[0-9]{6}-G_[0-9]{1,5}")){
		    newArr[k] = new Array(arr[i],tree2.getItemText(arr[i]),tree2.getUserData(arr[i],"desc"),tree2.getUserData(arr[i],"iden"));//id,name,desc,iden
		    k++;
		}
	}
	return newArr;	
}

function deleteTreeNode(itemId){
	if(itemId && itemId.indexOf("N_")==0){
	    tree2.deleteItem(itemId,true);
	}else{
	    alert("选中刚刚增加但未提交的节点才能删除");
	}
}
