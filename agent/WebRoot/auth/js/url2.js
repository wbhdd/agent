function onsubmitCheck(){
	if(checkNull('actionUrl','url名称')){
		return false;
	} else {
		var idsStr = tree2.getAllChecked();
		$$("newItems").value = idsStr;
		if(confirm("确定要提交吗?")){
			$$("dialogWindow").style.display = '';
			$$("urlForm").submit();
		}	
		return false;
	}
}

function onClickReflect(){
	if(confirm("反射将需要较长的时间，并且可能会影响系统运行，是否确定继续？")) {
		$$("dialogWindow").style.display = '';
		window.location='TAuthUrlDpAct.do?method=reflect';
	}
}

function confirmReflect() {
	if(confirm("反射将需要较长的时间，并且可能会影响系统运行，是否确定继续？")) {
		$$("dialogWindow").style.display = '';
		$$("urlForm").submit();
	}
}

function doDisplay() {
	$$("dialogWindow").style.display = '';
	intoAction('TAuthUrlDpAct.do?method=displayAssociation');
}
