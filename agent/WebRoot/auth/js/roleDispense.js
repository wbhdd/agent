function onsubmitCheck(){   

	var str = getItems($$("listChoosed"));
	if(str.length >= 0){
		$$("roleIds").value = str;
	}
	
	if(confirm("确定要提交吗?")){
		return true;
	}
	return false;
}

function moveItem(from, to){
    if(!from || !to){
    	alert("参数格式不符合要求");
    }
	var fm = document.forms[0];
	var fromSelectNode = $$(from);
	var toSelectNode = $$(to);
	
	if(fromSelectNode.selectedIndex<0){
		alert("请在列表中选择对应的项");
	} else {
		for(var k = 0;k<fromSelectNode.length;k++){
			if (fromSelectNode.options[k].selected){
				var opt = fromSelectNode.options[k];
				addItem(to, opt.text,opt.value);
			}
		}	
		deleteItem(from);
	}
}
function deleteItem(from){
	if(!from || arguments.length >= 2){
		alert("参数格式不符合要求");
	}
	var selectNode = $$(from);
	var n = selectNode.options.length-1;
	while(n>=0){
		if (selectNode.options[n].selected){
			selectNode.options.remove(n); 
		}
		n--;
	}
}
function addItem(to){
    if(!to || arguments.length % 2 ==0 ){
		alert("参数格式不符合要求");
	}
	if(arguments.length <=1) return ;
	
	var selectNode = $$(to);
	for(var i=1;i<arguments.length-1;i+=2) {
	    //判断时候已经有所选列表项已经在已选列表中，如是则不予添加。
		var opt = document.createElement("OPTION");
		opt.text = arguments[i];
		opt.value = arguments[i+1];
		
		flag = 0;
		for(var k=0;k<selectNode.length;k++){
			if (opt.value == selectNode.options[k].value){
				flag=flag+1;
			    break;
		    }
		}
		if (flag==0){
			selectNode.options.add(opt);
		}
	}
	return ;
}
function getItems(selectNode){
	var values="";	
	if(selectNode && selectNode.nodeName =="SELECT"){
		for(var n = selectNode.options.length-1;n>=0;n--){
		    var opt = selectNode.options[n];
		    if(values.length > 0) values = values + ",";
		    values = values + opt.value;
	    }
	}
	return values;
}
