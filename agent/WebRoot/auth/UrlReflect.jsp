<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>url修改</title>		
<script language="javascript" src="../js/mycommon.js"></script>
<script language="javascript" src="./js/url2.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">URL自动反射</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/TAuthUrlDpAct.do?method=doReflect">
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">执行操作</div>
    		<div id="tabtop-r1"></div>
    	</div>
  	</div>  	
  	<div id="main-tab">
	    <div id="info">	    
			<ul id="fh-flie-bdh">
	       		<li>
				<logic:equal property="auth.url.reflect.value" name="root" value="true" scope="session">	
					<html:button property="doAutoUrlReflect" styleClass="input1" value="确定反射" onclick="javascript:confirmReflect();"/>
				</logic:equal>
				<html:button property="cancel" styleClass="input1" value="  返回  " onclick="javascript:history.back();"/>
	       		</li>
	       	</ul>
		</div>
	</div> 	
	<div id="table">    	
		<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1">
		  
		  <tr>
			<td width="50%" valign="top">
				<div id="forSaveUrl" style="width:350; height:218;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;">
				  <table width="95%" align="center" cellpadding="5" cellspacing="1" class="table_query">
					<tr class="table_header">
					  <th colspan="4" align="left"> 需要增加的URL: 共${resultForSaveNum}项 </th>
					</tr>
					<tr class="table_header">
					  <th width="10%"><input type="checkbox" name="seleceAllForSave" onclick="fSelectAll2(this.name,'idsForSave');" /></th>
					  <th>ActionUrl</th>
					  <th>类型</th>
					</tr>
					<logic:present name="resultForSave" scope="request">
					  <logic:notEmpty name="resultForSave">
						<logic:iterate id="aRecord" name="resultForSave" scope="request">
						  <tr>
							<td align="center"><input type="checkbox" name="idsForSave" value="${aRecord.id}" /></td>
							<td align="center">${aRecord.actionUrl}</td>
							<td align="center"><html:select property="urlType" value="${aRecord.urlType}" disabled="true">
								<html:option value="1">strut action类</html:option>
								<html:option value="2">普通jsp</html:option>
								<html:option value="3">特别url</html:option>
							</html:select></td>
						  </tr>
						</logic:iterate>
					  </logic:notEmpty>
					</logic:present>
				  </table>
				</div><!--forSaveUrl-->
			</td>
			<td width="50%" valign="top">
				<div id="forDeleteUrl" style="width:350; height:218;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;">	
				<table width="95%" align="center" cellpadding="5" cellspacing="1" class="table_query">
				  <tr class="table_header"> 
					<th colspan="4" align="left">
						需要删除的URL: 共${resultForDeleteNum}项						</th>
				  </tr>
				  <tr class="table_header">  
					<th width="10%"><input type="checkbox" name="seleceAllForDelete" onclick="fSelectAll2(this.name,'idsForDelete');" /></th>  
					<th>ActionUrl</th>
					<th>类型</th> 
				  </tr>	
					<logic:present name="resultForDelete" scope="request">
						<logic:notEmpty name="resultForDelete">
							<logic:iterate id="aRecord" name="resultForDelete" scope="request">		  
							  <tr>
								<td align="center"><input type="checkbox" name="idsForDelete" value="${aRecord.id}" /></td>
								<td align="center">
								  <a href="TAuthUrlDpAct.do?method=displayAssociation&id=${aRecord.id}" target="_blank">
									${aRecord.actionUrl}(
									  <logic:equal name="aRecord" property="itemNum" value="0">
										${aRecord.itemNum}										  
									  </logic:equal>
									  <logic:notEqual name="aRecord" property="itemNum" value="0">
										<span class="red">${aRecord.itemNum}</span>										  
									  </logic:notEqual>
									)									  
								  </a>									
								</td>
								<td align="center"><html:select property="urlType" value="${aRecord.urlType}" disabled="true">
									<html:option value="1">strut action类</html:option>
									<html:option value="2">普通jsp</html:option>
									<html:option value="3">特别url</html:option>
								  </html:select></td>
							  </tr>
							</logic:iterate>
						</logic:notEmpty>
					</logic:present> 
				</table>
				</div><!--resultForDelete-->
			  </td>
		  </tr>
		</table>
	</div>
</html:form>
</div>
</body>
</html>
<%@ include file="Dialog.jsp" %>
