<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>权限用户列表</title>
<script language="javascript" src="../js/mycommon.js"></script>
<script>
function dispense(id) {
	$$('dialogWindow').style.display='';
	userForm.action="../auth/TUserDpAct.do?method=dispense&&id=" + id;
	userForm.submit();
}
</script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">权限用户管理</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/TUserDpAct.do?method=list">
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">查询条件</div>
    		<div id="tabtop-r1"></div>
    	</div>
  	</div>  	
  	<div id="main-tab">
	    <div id="info">	    
			<ul id="fh-flie-bdh">
	       		<li>
	       			userid：<html:text property="userNetId" size="40"/>
	       		</li>
	       		<li>
	       			<html:submit styleClass="search" property="search" value=" 查询 "/>
	       		</li>
	       	</ul>
		</div>
	</div>
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">查询结果</div>
    		<div id="tabtop-r1"></div>
			<!---
			权限的用户是与项目的用户一致的，在这里不需要做增加、删除
			<logic:equal property="auth.user.delete.value" name="root" value="true" scope="session">
	    		<li><html:button styleClass="tabny-l" value="删除选中"property="deleteChecked" onclick="return deleteSelected('TAuthUserDpAct.do?method=doDelete');"/></li>
			</logic:equal>
			<logic:equal property="auth.user.add.value" name="root" value="true" scope="session">				
    			<li><html:button  styleClass="tabny-l-1" value="添 加" property="addRecord" onclick="window.location='TAuthUserDpAct.do?method=add'"/></li>
			</logic:equal>
			--->
    	</div>
    </div>
    
    <div id="fh-flie-2"></div>	
	<div id="main-tablist">
  	<table width="100%" align="center" cellpadding="0" cellspacing="0" >
    <!---------------表格列表----------------------------------------->  	
	<tr id="minpt-tab">  
	    <td width="8%"><input type="checkbox" name="seleceAll" onclick="fSelectAll(this.name);" /></td>
	    <td>id</td>
	    <td>userNetId</td>
	    <td width="15%">修改</td>
  	</tr>
	<logic:present name="result" scope="request">
		<logic:notEmpty name="result">
			<logic:iterate id="aRecord" name="result" scope="request">		  
			  <tr class="out" onmouseover="this.className='over'" onmouseout="this.className='out'">
				<td align="center"><input type="checkbox" name="ids" value="${aRecord.id}" /></td>
				<td align="center">${aRecord.id}</td>
			    <td align="center">${aRecord.userNetId}</td>
			    <td>
					<logic:equal property="auth.user.association.value" name="root" value="true" scope="session">
						<a href="javascript:dispense('${aRecord.id}')">持有角色</a>				
					</logic:equal>				
				</td>
			  </tr>
			</logic:iterate>
		</logic:notEmpty>
	</logic:present>
</table>
  <div id="info-pz">
    ${link}
  </div>
</div>
</html:form>
</div>
</body>
</html>
<%@ include file="Dialog.jsp" %>
