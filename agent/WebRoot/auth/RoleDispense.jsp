<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>为用户分配角色</title>
	
<script language="javascript" src="./js/roleDispense.js"></script>
<script language="javascript" src="../js/mycommon.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

</head>

<body>
	<div id="main">
			
	<div id="tab-top">
  <div id="ptk1">
  <div id="lift"></div>
  <div id="pt">角色分发</div>
  <div id="right"></div>
  </div>
  </div>
<html:form action="/auth/TUserDpAct.do?method=doDispense" onsubmit="return onsubmitCheck();">
<!--权限表t_user中的id值，t_user_role中的用户id就是该值-->
<input type="hidden" name="id" value="${result.id}"/>
<input type="hidden" name="roleIds" value="${roleIds}"/>
	<table width="100%">
	  <tr align="left"><td><div align="center">帐号：${result.userNetId}</div></td>
	  </tr>
	</table>	
	<div id="main-tab">
		<div id="info">
		<table width="100%">
		  <tr>
			<td valign="top" width="29%">未持有的角色列表<br />
			<select multiple name="listAll"  size="20" id="select" style="width:300px">
			  <logic:present name="listUnHold">
				  <logic:iterate id="theRecord" name="listUnHold">
					<option value="${theRecord.id}">${theRecord.roleName}</option>
				  </logic:iterate>
			  </logic:present>  
			</select>
			</td>
			<td valign="center" width="6%">
				<input  type="button" alt="对用户分配角色" name="Submit3" value="&gt;&nbsp;分配&nbsp;&gt;" onClick="moveItem('listAll', 'listChoosed');"  class="search"><br /><br />
				<input  type="button" alt="对用户删除角色" name="Submit4" value="&lt;&nbsp;删除&nbsp;&lt;" onClick="moveItem('listChoosed', 'listAll');"  class="search"><br />
			</td>
			<td width="65%" valign="top">已经持有的角色列表<br />
			<select multiple name="listChoosed"  size="20" id="list" style="width:300px">			
			  <logic:present name="listHolding">
				  <logic:iterate id="theRecord" name="listHolding">
					<option value="${theRecord.id}">${theRecord.roleName}</option>
				  </logic:iterate>
			  </logic:present>   
			</select>
			</td>
		  </tr>
		  <tr><td align="center"></td>
			  <td align="center">
			  	<logic:equal property="auth.user.association.value" name="root" value="true" scope="session">
				  <input type="submit" value="确定修改"  class="search"/>
				</logic:equal>
			  </td>
			  <td align="left"><input type="button" value="返回" onclick="javascript:history.back();"  class="search"/></td>
		  </tr>
		</table>
        </div>
  </div>
</html:form>
</div>
</body>
</html>

