<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>权限删除</title>
		

	
<script language="javascript" src="./js/tree/dhtmlXCommon.js"></script>
<script language="javascript" src="./js/tree/dhtmlXTree.js"></script>	

<script language="javascript" src="./js/authCommon.js"></script>
<script language="javascript" src="./js/authorityDelete.js"></script>
<script language="javascript" src="../js/mycommon.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">权限删除</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/AuthorityDpAct?method=doDelete" onsubmit="return onsubmitCheck();" >
<html:hidden property="ids"/>
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">权限删除</div>
    		<div id="tabtop-r1"></div>
    		<li>&nbsp;&nbsp;&nbsp;选中左边权限, 然后提交</li>
    	</div>
  	</div>  	
  	<div id="main-tablist">		
	
	<table width="100%">
      <tr>
        <td valign="top" width="30%"><div id="treeboxbox_tree2" style="width:350; height:218;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"></div></td>
        </tr>
	  <tr>
	    <td id="stateBar"></td>
	    </tr>
	  <tr>
		<td align="center">
			<html:button styleClass="ny-2-1" property="cancel" value=" 返回 " onclick="javascript:history.back();"/>		
			<logic:equal property="auth.auth.delete.value" name="root" value="true" scope="session">
				<html:submit styleClass="input1" property="add" value="确定删除"/>			
			</logic:equal>
	  	</td>
	  </tr>
    </table>
    
    </div>
		
</html:form>
	<script language="javascript">
			var tree2=new dhtmlXTreeObject("treeboxbox_tree2","100%","100%",0);
			tree2.setImagePath("./imgs/");
			tree2.enableCheckBoxes(1);
			tree2.enableThreeStateCheckboxes(true);
			tree2.loadXML("authTree.xml");
			tree2.setOnClickHandler(doOnClick);
	
	        function doOnClick(nodeId){
			    var name = tree2.getItemText(nodeId);
				var desc = tree2.getUserData(nodeId,"desc");
				var iden = tree2.getUserData(nodeId,"iden");			
				var str = ' id：'+ nodeId +'\t\n 名称：'+ name +'\t\n 描述：'+ desc +'\t\n 标识：'+ iden;
				var stateBar = document.getElementById('stateBar');
				stateBar.innerHTML=str;
            }			
	</script>
</div>
</body>
</html>
