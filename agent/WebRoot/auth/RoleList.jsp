<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>角色列表</title>
<script language="javascript" src="../js/mycommon.js"></script>


<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>
	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">角色管理</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/TRoleDpAct?method=list">
	<div id="table">
	   	<div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">查询条件</div>
	    	<div id="tabtop-r1"></div>
	    </div>
	</div>
    
    		<div id="main-tab">
			  	<div id="info">	    
					<ul id="fh-flie-bdh">
			       		<li>
			       			角色名称：<html:text property="roleName"/>
						</li>
						<li>类型:
							<html:select property="roleType">
							  <logic:present name="roleTypeList">
								<logic:iterate id="theRecord" name="roleTypeList">
								  <html:option value="${theRecord.key}">${theRecord.value}</html:option>
								</logic:iterate>
							  </logic:present>			  
							</html:select>									
						</li>
						<li>
							<html:submit styleClass="search" property="search" value=" 查询 "/>
						</li>
					</ul>
    			</div>
  			</div>  	

	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">查询结果</div>
    		<div id="tabtop-r1"></div>
			<logic:equal property="auth.role.delete.value" name="root" value="true" scope="session">
	    		<li><html:button value="删除选中" styleClass="tabny-l" property="deleteChecked" onclick="if(deleteSelected('../auth/TRoleDpAct.do?method=delete') == false) {return false} else {$$('dialogWindow').style.display = '';}"/></li>			
			</logic:equal>
			<logic:equal property="auth.role.add.value" name="root" value="true" scope="session">
    			<li><html:button  styleClass="tabny-l-1" value="添 加" property="addRecord" onclick="window.location='../auth/TRoleDpAct.do?method=add'"/></li>
			</logic:equal>			
    	</div>
    </div>
    
    <div id="fh-flie-2"></div>	
	<div id="main-tablist">
  	<table width="100%" align="center" cellpadding="0" cellspacing="0" >
      <tr >
	   <td height="1" colspan="5">
       	<table width="760" border="0" cellspacing="0" cellpadding="0" height="1" >
          	<tr>
				<td></td>
      		</tr>
		</table>
      </td>
    </tr>
    <!---------------表格列表----------------------------------------->  	
	<tr id="minpt-tab">
	    <td width="10%"><input type="checkbox" name="seleceAll" onclick="fSelectAll(this.name);" /></td>
	    <td>角色名称(关联权限)</td>
	    <td>角色说明</td>
	    <td>角色类型</td>
	    <td>操作</td>
  	</tr>
	<logic:present name="result" scope="request">
		<logic:notEmpty name="result">
			<logic:iterate id="aRecord" name="result" scope="request">		  
			  <tr class="out" onmouseover="this.className='over'" onmouseout="this.className='out'">
				<td align="center"><input type="checkbox" name="ids" value="${aRecord.id}" /></td>
				<td align="center">${aRecord.roleName}(${aRecord.itemNum})</td>
				<td align="center">${aRecord.roleDesc}</td>
				<td align="center">
					<html:select property="roleType" value="${aRecord.roleType}">
					  <logic:present name="roleTypeList">
						<logic:iterate id="theRecord" name="roleTypeList">
						  <html:option value="${theRecord.key}">${theRecord.value}</html:option>
						</logic:iterate>
					  </logic:present>			  
		    		</html:select>				
				</td>
			  <td>
			  	<logic:equal property="auth.role.modify.value" name="root" value="true" scope="session">
					<a href="javascript:intoAction('../auth/TRoleDpAct.do?method=modify&id=${aRecord.id}')" title="修改角色所拥有的权限"><img src="../themes/images/ico/cog.gif" width="16" height="16" class="icoimg" />修改</a>&nbsp;&nbsp
				</logic:equal>
				<logic:equal property="auth.role.association.value" name="root" value="true" scope="session">
					<a href="javascript:intoAction('../auth/TRoleDpAct.do?method=displayAssociation&id=${aRecord.id}')" title="修改已关联帐号"><img src="../themes/images/ico/accept.gif" width="16" height="16" class="icoimg" />关联帐号(${aRecord.userNum})</a>
				</logic:equal>
			  </td>
			  </tr>
			</logic:iterate>
		</logic:notEmpty>
	</logic:present>
</table>
  <div id="info-pz">
    ${link}
  </div>
</div>
</html:form>
</div>
</body>
</html>
<%@ include file="Dialog.jsp" %>
