<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>修改个人资料</title>

    <link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	<script type="text/javascript" src="../themes/js/jquery.js"></script>
	


</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">修改个人信息</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="table">
	    <div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">输入信息</div>
	    	<div id="tabtop-r1"></div>
      </div>
	</div>
	<div id="main-tab">		
		<html:form action="/Logon/LogonDpAct?method=doModifySelfInfo" method="post">	
		<table width="200" align="center" class="table-slyle-hs" >
			<tr>
				<td colspan="2">
					<table width="300" border="0" align="center">
			  <tr>
				<td width="100">密码</td>
				<td><html:text property="password" value="${attribute.password}" /></td>
			  </tr>
			  <tr>
				<td width="100">昵称</td>
				<td><html:text property="nickName" value="${attribute.nickName}" /></td>
			  </tr>
			  <tr>
				<td width="100">真实姓名</td>
				<td><html:text property="name" value="${attribute.name}" /></td>
			  </tr>
			  <tr>
				<td width="100">电话号码</td>
				<td><html:text property="phone" value="${attribute.phone}" /></td>
			  </tr>
			  <tr>
				<td width="100">EMAIL</td>
				<td><html:text property="email" value="${attribute.email}" /></td>
			  </tr>

					  <tr>
						<td colspan="2"><div align="center"><input type="submit" class="search-2" value="确定修改"/></div></td>
					  </tr>					  
					</table>
				</td>
			</tr>		
		</table>
		</html:form>
	</div>

</div>
</body>
</html>
