<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>top</title>


<link rel="stylesheet" href="../themes/skyblue/skyblueTop.css" type="text/css" title="styles1" />

<script type="text/javascript" src="../plugins/jquery-x.x.js"></script>
<script type="text/javascript" src="../js/online/agent-postman.js"></script>
<script type="text/javascript" src="../js/online/constant.js"></script>
		
<script type="text/javascript">
var beater;
var beaterError = 0;
function beat() {
	beater = setInterval(function () {
		$.ajax({type:"GET", url:"LogonDpAct.do?method=beat", success:function (resp) {
		}, error:function(resp){
			beaterError ++;
			if(beaterError == 5) {
				clearInterval(beater);
			}
		}});
	}, 60000);
}
beat();

function count() {
	//处于坐席界面，不需要检测
	if(window.top.frames["mainFrame"].location.href.indexOf("AgentDpAct.do?method=online") > 0){
		return;
	}
	getCount('${attribute.username}', function(resp) {
		if(resp.msg != "0"){
			$("#agentService").html("客服[" + resp.msg + "]");
			if(!colorInterval) {
				changeColor();
			}
			flashTitle(true);
		} else {
			stopChangeColor();
		}
	});
}
//如果坐席点击进入服务，则开始每五秒钟调用cuont检测一次是否有新消息（假设客服离开服务界面也能知道有新的消息）
var countInterval;
function toService(){
	//点击之后按钮就不用再闪缩了
	stopChangeColor();
	stopFalshTitle();
	
	window.top.frames["mainFrame"].location = "../agent/AgentDpAct.do?method=online";
	if(!countInterval) {
		countInterval = setInterval(count, 5000);
	}
}

//如果不在服务界面，检测知道有新的消息存在则回闪缩颜色
var colorInterval;
function changeColor(){
	colorInterval = setInterval(
		function(){
			var cl = "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6);
			$("#agentService").css("background-color", cl)
		}
	, 2000);
}
//点击进入客服服务，则又要停止颜色闪缩
function stopChangeColor(){
	if(colorInterval) {
		clearInterval(colorInterval);
		colorInterval = null;
	}
	$("#agentService").html("客服");
	$("#agentService").css("background-color", "");
}
///////////////////////title闪烁
var originalTitle = parent.document.title;
var step=0;
var flash_title_timer;
function flashTitle() {
  if(!flash_title_timer) {
	  flash_title_timer = setInterval(function () {
			flashTitle();
		}, 800);
  }
	
  step++;
  if (step==3) { step=1 } 
  if (step==1) { parent.document.title='[' + TEXT.HAVE_NEW_MESSAGE + ']' }
  if (step==2) { parent.document.title='[' + TEXT.AGENT_SERVICE + ']' }
}
function stopFalshTitle() {
	if(flash_title_timer) {
		clearTimeout(flash_title_timer);
		flash_title_timer = null;
	}
	parent.document.title = originalTitle;
}

</script>
</head>
<body >
<div id="top">
  <div id="toolbar">
    <div id="bar">
      <div id="user"></div>
      <div id="userinfo">
        <ul>
          <li> 工号：${attribute.no}</li>
        </ul>
      </div>
      <div id="userinfo2">
        <ul class="userlink">
          <li class="close"><a href="LogonDpAct.do?method=logout" target="_parent">退出系统</a></li>
          <li><a href="ModifySelfInfo.jsp" target="mainFrame">个人资料</a></li>
          <li><a href="ModifySelfPassword.jsp" target="mainFrame">修改密码</a></li>
          <li class="close"><a href="javascript:"><span id="agentService" onclick="toService()">进入客服</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</body>
</html>
