<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage=""%>
<div id="content">
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>基础信息</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../base/Org.jsp" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />企业信息 </a>
			</li>
			<li>
				<a href="../base/TChannelDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />渠道管理 </a>
			</li>
		</ul>
	</div>

	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>账号信息</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../user/TUserDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />账号管理 </a>
			</li>
			<li>
				<a href="../user/TUserDpAct.do?method=list&type=a" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />坐席查询 </a>
			</li>
			<li>
				<a href="../user/TGroupDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />技能组管理 </a>
			</li>
			<li>
				<a href="../self/TQuestionDpAct.do?method=list&tag=SELF&level=1&type=B" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />坐席标准用语 </a>
			</li>
		</ul>
	</div>

	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>参数配置</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=SERVICE_TIME&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />服务时间 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=TIP&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />提示语 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=ENTER&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />接入配置 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=LIMIT&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />时效配置 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=AGENT&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />坐席配置 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=SATISFY&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />评价配置 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=SELF&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />自助配置 </a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>自助聊天</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=SELF&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />自助配置 </a>
			</li>
			<li>
				<a href="../self/TQuestionDpAct.do?method=list&level=1&type=F" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />自助问答 </a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>UI管理</a>
		</h3>
	</div>
	<div class="stretcher2">
		<li>
			<a href="../base/TSettingDpAct.do?method=list&tag=UI&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />UI配置 </a>
		</li>
		<ul>
			<li>
				<a href="../self/TButtonDpAct.do?method=list&level=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />面板按钮 </a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>客户信息</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../crm/TCrmDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />客户管理 </a>
			</li>
			<li>
				<a href="../crm/TCrmDpAct.do?method=list&type=1" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />对接客户</a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>业务数据</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../biz/TSessionDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />访客会话记录 </a>
			</li>
			<li>
				<a href="../biz/TAgentSessionDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />坐席会话记录 </a>
			</li>
			<li>
				<a href="../biz/TLwDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />留言信息 </a>
			</li>
			<li>
				<a href="../biz/TTraceDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />访客访问轨迹</a>
			</li>			
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>工单数据</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../biz/TWorderDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />所有工单</a>
			</li>
			<li>
				<a href="../biz/TWorderDpAct.do?method=list&status=SLZ" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />受理中工单</a>
			</li>
			<li>
				<a href="../biz/TWorderDpAct.do?method=list&status=YJJ" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />已解决工单</a>
			</li>
			<li>
				<a href="../biz/TWorderDpAct.do?method=list&status=YGB" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />已关闭工单</a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>系统扩展</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=EXTEND" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />扩展基础 </a>
			</li>
			<li>
				<a href="../base/TSettingDpAct.do?method=list&tag=EXTEND2" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />访客端扩展 </a>
			</li>
			<li>
				<a href="../extend/TExtendDpAct.do?method=list" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />座席端扩展 </a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>监控</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../monitor/pandect.jsp" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />总览 </a>
			</li>
			<li>
				<a href="../monitor/agent.jsp" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />坐席监控 </a>
			</li>
			<li>
				<a href="../monitor/session.jsp" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />会话监控 </a>
			</li>
		</ul>
	</div>
	<div id="meunacrbg">
		<h3 class="display">
			<a href="#"><div class="img"></div>统计</a>
		</h3>
	</div>
	<div class="stretcher2">
		<ul>
			<li>
				<a href="../monitor/ReporterDpAct.do?method=pandect" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />总览 </a>
			</li>
			<li>
				<a href="../monitor/ReporterDpAct.do?method=agents" target="mainFrame"> <img src="../themes/images/ico/user_add.gif" width="16" height="16" class="icoimg" />坐席工作量统计 </a>
			</li>
		</ul>
	</div>
</div>
