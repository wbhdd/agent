
function vl(value) {
    value = $.trim(value);
    return value != null && value != 'undefined' && value !== '' && !(typeof value == 'undefined') && value !== " ";
}


function getForm(myFormName) {
	var forms = document.getElementsByTagName("form");
	if (forms.length > 1) {
		if (!myFormName) {
			return forms[0];
		} else {
			forms = document.getElementsByName(myFormName);
			if (forms.length > 1) {
				return false;
			} else if (forms.length < 1) {
				return false;
			}
		}
	} else if (forms.length < 1) {
		return false;
	}
	return forms[0];
}

function checkAllInput() {
	var allOk = true;
	$(".mytip").remove();
	$(":input").each(function() {
		var require = $(this).attr("require");
		if (!vl(require)) {// 没有任何要求
		} else {
			var requires = require.split(";");
			for (var v = 0; v < requires.length; v++) {
				var r = requires[v];
				if (r == "require") {
					if (!vl($(this).val())) {
						allOk = false;
						$(this).after("<span class='mytip'>必填</span>");
						return;
					}
				} else if (r == "number") {
					var string = "1234567890.";
					var value = $(this).val();
					for (var i = 0; i < value.length; i++) {
						var ch = value.charAt(i);
						if (string.indexOf(ch) < 0) {
							allOk = false;
							$(this).after("<span class='mytip'>数字</span>");
							return;
						}
					}
				}
			}
		}
	});
	return allOk;
}

function ajaxSubmit(form) {
	var f = getForm(form);
	if (f == false) {
		layer.msg("请给提交增加一个Form表单");
		return false;
	}
	if (checkAllInput() == false) {
		layer.msg("数据校验不完全，请根据提示填写完整");
		return false;
	}
	if (typeof beforeAjax == "function") {
		if (beforeAjax() == false) {
			return false;
		}
	}
	layer.confirm('是否确认操作？ ', {
		title : '确认操作',
		shadeClose : true,
		btn : [ '确认', '不确认' ]
	}, function() {
		var url = $(f).attr('action');
		$.ajax({
			url : url,
			data : $(f).serializeArray(),
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				layer.closeAll();
				if (typeof callbackSuccess == "function") {
					callbackSuccess(data)
				} else {
					layer.msg("提交成功");
				}
			},
			error : function(data) {
				layer.closeAll();
				if (typeof callbackError == "function") {
					callbackError(data)
				} else {
					layer.msg("提交失败");
				}
			}
		});
	});
}

function uploadFile(token, url, form) {
	var f = getForm(form);
	if (f == false) {
		layer.msg("请给提交增加一个Form表单");
		return false;
	}	
	if (typeof beforeUpload == "function") {
		if (beforeUpload() == false) {
			return false;
		}
	}
	var params = {
		token : token,
		uuid : "uuid-" + (new Date().getTime()),
		bizType : "SETTING",
		uploadUrl : url
	};
	MyUpload.uploadFile(params, $(f), function(resp) {		
		uploadResult(resp);
	});
}


function layerConfirm(tip, url, data, callbackf) {
	layer.confirm(tip, {
		title : '确认操作',
		shadeClose : true,
		btn : [ '确认', '不确认' ]
	}, function() {
		$.ajax({
			url : url,
			data : data,
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				layer.closeAll();
				if (typeof callbackf == "function") {
					callbackf(data)
				} else {
					layer.msg("操作成功");
				}
			},
			error : function(data) {
				layer.closeAll();
				if (typeof callbackf == "function") {
					callbackf(data)
				} else {
					layer.msg("操作失败");
				}
			}
		});
	});
}