function showInviting() {
	var visitor = talkingVisitor[currentVisitorId];
	if(visitor.inviting == true) {
		$("#inviting").show();
	} else {
		$("#inviting").hide();
	}
}

function inviteAgent(agent, describe) {
	layer.closeAll();
	showLayer(AGENT_TEXT.INVITING + agent);
	var visitor = talkingVisitor[currentVisitorId];
	visitor.inviting = true;//记录为邀请中
	var sid = visitor.object.session.id;
	var data = {event:MESSAGE_EVENT.AG_INVITE_AGENT, value:agent, value2:describe, sid : sid};
	send(data, function (resp) {
		if (resp.code == 0) {
			showLayer(TEXT.DO_SUCCESS);
		} else {
			showLayer(TEXT.DO_ERROR + ":" + resp.msg);
		}
		get();
	});
}

function transfer2Agent(agent, describe) {
	layer.closeAll();
	showLayer(AGENT_TEXT.TRANSFERING + agent);
	var visitor = talkingVisitor[currentVisitorId];
	visitor.transfering = true;//记录为邀请中
	var sid = visitor.object.session.id;
	var data = {event:MESSAGE_EVENT.AG_TRANSFER2_AGENT, value:agent, value2:describe, sid : sid};
	send(data, function (resp) {
		if (resp.code == 0) {
			showLayer(TEXT.DO_SUCCESS);
		} else {
			showLayer(TEXT.DO_ERROR + ":" + resp.msg);
		}
		get();
	});
}


//接入邀请
function acceptInvite(cid) {
	var session = waitingVisitor[cid].object.session;
	var data = {event:MESSAGE_EVENT.AG_ACCEPT_INVITE, sid : session.id};
	send(data, function (resp) {
		if (resp.code == 0) {//成功只有移除li,并且会收到invite notify,其他人也会收到join session的通知
			removeWaitingVisitor(waitingVisitor[cid].object.crm);
			showLayer(TEXT.DO_SUCCESS);
			unBlockInput();
		} else {
			showLayer(TEXT.DO_ERROR + ":" + resp.msg);
		}
		get();
	});
}
//拒接邀请
function rejectInvite(cid) {
	var session = waitingVisitor[cid].object.session;
	var data = {event:MESSAGE_EVENT.AG_REJECT_INVITE, sid : session.id};
	send(data, function (resp) {
		if (resp.code == 0) {
			removeWaitingVisitor(waitingVisitor[cid].object.crm);
			showLayer(TEXT.DO_SUCCESS);
			unBlockInput();
		} else {
			showLayer(TEXT.DO_ERROR + ":" + resp.msg);
		}
		get();
	});
}

function cancelInvite() {
	var session = waitingVisitor[theVisitor.cid].object.session;
	//不管是否操作成功，客户端都认为成功标记为取消邀请
	var visitor = talkingVisitor[currentVisitorId];
	visitor.inviting = false;
	var data = {event:MESSAGE_EVENT.AG_CANCEL_INVITE, cid : session.id};
	send(data, function (resp) {
		if (resp.code == 0) {
			showLayer(TEXT.DO_SUCCESS);
		} else {
			showLayer(TEXT.DO_ERROR + ":" + resp.msg);
		}
		get();
	});
}

function inviteCanceled() {
	
}


function inviteRejected() {
	
}

function inviteAccepted() {
	
}