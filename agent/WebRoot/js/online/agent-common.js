function showLogs(sid) {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "聊天历史",
		content : "../online/agent.jsp?onlyShow=true&sid=" + sid,
		move : false,
		maxmin : true,
		area : [ '80%', '80%' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
}