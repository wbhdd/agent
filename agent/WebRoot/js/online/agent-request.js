
function getLog(cid, manual) {
	var pageCount = 15;
	if (!vl(pgData[cid])) {
		pgData[cid] = 1;
	} else {
		pgData[cid] = pgData[cid] + 1;
	}
	get("crmLog", {cid:cid, start:pgData[cid], count:pageCount}, false, function (resp) {
		var messages = resp;
		for (var v = 0; v < messages.length; v++) {
			var message = messages[v];
			showChatMessge(message, "prepend");
		}
		if (messages.length > 0) {
			showMoreMessage(cid);
		}
		if (messages.length < pageCount) {
			hideMoreMessage();
		}
		if (!vl(manual)) {
			toBottom();
		}
	});
}
function getLogBySid(sid) {
	$("#msgHolder").html("");
	get("crmLog", {sid : sid, start:1, count:1000}, false, function (resp) {
		var messages = resp;
		for (var v = 0; v < messages.length; v++) {
			var message = messages[v];
			showChatMessge(message, "prepend");
		}
		toBottom();
	});
}


function handleLw(lid) {
	commit("../biz/TLwDpAct.do?method=doModify", {id : lid, status : "Y", handleResult : $("#handleResult").val()});
	return true;
}
function getSetting() {
	get("setting", {}, false, function (resp) {
		setting = resp;
		showSetting(setting);
	});
}
function getTalkingVisitor() {
	get("talking", {}, false, function (resp) {
		sessions = resp;
		for (var v = 0; v < sessions.length; v++) {
			var one = sessions[v];
			talkingVisitor[one.object.crm.id] = one;
			showTalkingVisitor(one);
		}
		$("#talkingCount").html("[" + Object.getOwnPropertyNames(talkingVisitor).length + "]");
		selectTheFirst();
	});
}
function getWaitingList() {
	get("waitingList", {}, false, function (resp) {
	});
}
function getOtherTalkingVisitor() {
	get("otherTalking", {}, false, function (resp) {
		sessions = resp;
		for (var v = 0; v < sessions.length; v++) {
			var one = sessions[v];
			talkingVisitor[one.object.crm.id] = one;
			showTalkingVisitor(one);
		}
		$("#talkingCount").html("[" + Object.getOwnPropertyNames(talkingVisitor).length + "]");
		selectTheFirst();
	});
}

function getLwNoHandled() {
	get("lwNoHandled", {}, true, function (resp) {
		sessions = resp;
		if(sessions) {
			for (var v = 0; v < sessions.length; v++) {
				var one = sessions[v];
				showLwNoHandled(one);
			}
			$("#noHandledCount").html("[" + sessions.length + "]");
		} else {
			$("#noHandledCount").html("[0]");
		}
	});
}

function getLwHandled() {
	get("lwHandled", {}, true, function (resp) {
		sessions = resp;
		if(sessions) {
			for (var v = 0; v < sessions.length; v++) {
				var one = sessions[v];
				showLwHandled(one);
			}
			$("#handledCount").html("[" + sessions.length + "]");
		} else {
			$("#handledCount").html("[0]");
		}
	});
}
function getHistoryVisitor() {
	get("history", {}, false, function (resp) {
		sessions = resp;
		for (var v = 0; v < sessions.length; v++) {
			var one = sessions[v];
			historyVisitor[one.object.crm.id] = one;
			historyVisitor[one.object.crm.id].visitorStatus = MEMBER_STATUS.HISTORY;
			showHistoryVisitor(one);
		}
		$("#historyCount").html("[" + Object.getOwnPropertyNames(historyVisitor).length + "]");
	});
}
function getAgent() {
	get("agent", {}, false, function (resp) {
		agentSelfInfo = resp;
		showAgent(agentSelfInfo);
	});
}
function getGroupsAndMembers() {
	get("groupsAndMember", {}, false, function (resp) {
		groupsAndMembers = resp;
		if (groupsAndMembers.length == 0) {
			showLayer(AGENT_TEXT.NO_ANY_GROUP);
		} else {//
			for (var v = 0; v < groupsAndMembers.length; v++) {
				var groupId = groupsAndMembers[v][0].id;
				if (!allGroup[groupId]) {
					allGroup[groupId] = {};
					allGroup[groupId].group = groupsAndMembers[v][0];
					allGroup[groupId].member = new Array();
				}
				allGroup[groupId].member.push(groupsAndMembers[v][1]);
			}
			showGroupsAndMembers(allGroup);
			var groups = "";
			for (var one in allGroup) {
				var groupId = allGroup[one].group.id;
				groups = groups + groupId + ",";
			}
			joinGroups(groups);
		}
	});
}
function changeStatus() {
	var data = {event:MESSAGE_EVENT.AG_CHANGE_STATUS, value:$("#serviceStatus").val()};
	send(data, function (resp) {
		if (resp.code == 0) {
			showLayer(TEXT.DO_SUCCESS);
		} else {
			showLayer(TEXT.DO_ERROR);
		}
		get();
	});
}
function changeMaxChat() {
	var data = {event:MESSAGE_EVENT.AG_CHANGE_MAXCHAT, value:$("#maxChat").val()};
	send(data, function (resp) {
		if (resp.code == 0) {
			showLayer(TEXT.DO_SUCCESS);
		} else {
			showLayer(TEXT.DO_ERROR);
		}
		get();
	});
}
function receiveSession() {
	var data = {event:MESSAGE_EVENT.AG_RECEIVE_SESSION, sid:theQueue[currentQueueVisitorId].sid};
	send(data, function (resp) {
		layer.closeAll();
		showButtons("", "");
		if (resp.code == 0) {
			showLayer(TEXT.DO_SUCCESS);
		} else {
			showLayer(TEXT.DO_ERROR);
		}
		get();
	});
}
function joinGroups(groups0) {
	var data = {event:MESSAGE_EVENT.AG_JOIN_GROUP, value:groups0};
	send(data, function (resp) {
		layer.closeAll();
		if (resp.code == 0) {
			showLayer(AGENT_TEXT.JOIN_GROUP_SUCCESS);
			get();
		} else {
			showLayer(TEXT.DO_ERROR);
		}
	});
}
function endSession() {
	layer.confirm(TEXT.CONFIRM_END_SESSION, {title:TEXT.CONFIRM_TITLE, shadeClose:true, btn:[TEXT.CONFIRM, TEXT.CANCEL]}, function () {
		$("#btnEnd").hide();
		var data = {event:MESSAGE_EVENT.AG_END_SESSION, cid:currentVisitorId};
		send(data, function (resp) {
			layer.closeAll();
			if (resp.code == 0) {
				showLayer(TEXT.DO_SUCCESS);
			} else {
				showLayer(TEXT.DO_ERROR);
			}
			get();
		});
	});
}
function httpLogoutClick() {
	layer.confirm(AGENT_TEXT.AG_LOGOUT, {title:TEXT.CONFIRM_TITLE, shadeClose:true, btn:[TEXT.CONFIRM, TEXT.CANCEL]}, function () {
		var data = {event:MESSAGE_EVENT.AG_LOGOUT, cid:currentVisitorId};
		send(data, function (resp) {
			layer.closeAll();
			if (resp.code == 0) {
				myClearInterval();
				window.location='../base/Org.jsp';
			} else {
				showLayer(TEXT.DO_ERROR);
			}
			get();
		});
	});
}

